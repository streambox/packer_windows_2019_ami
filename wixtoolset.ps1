choco install wixtoolset

@'
$dir_path = (gci -r C:\Program*\Wix*Toolset*\bin -Filter "heat.exe" | select-object -first 1).Directory.FullName
$path = [Environment]::GetEnvironmentVariable("PATH", "Machine")
$prefix = @{$true='';$false=';'}[$path -eq '']
[Environment]::SetEnvironmentVariable("PATH", $path + "${prefix}${dir_path}", "Machine")
'@ | Out-File -Encoding ASCII C:\Windows\Temp\add_wixtoolset_to_path.ps1

C:\Windows\Temp\add_wixtoolset_to_path.ps1
