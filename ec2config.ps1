Import-Module 7Zip4Powershell

New-Item -Force -Type "directory" C:\Windows\Temp\EC2Config | Out-Null

$url = [System.Uri]"https://s3.amazonaws.com/ec2-downloads-windows/EC2Config/EC2Install.zip"
$zip = Join-Path $pwd (Split-Path -Leaf -Path $url.AbsolutePath)

Invoke-WebRequest -OutFile $zip $url
Expand-Archive -Force -LiteralPath $zip -DestinationPath C:\Windows\Temp\EC2Config

C:\Windows\Temp\EC2Config\Ec2Install.exe /install /norestart /passive
