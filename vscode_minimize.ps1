$basedir = $pwd
$basedir = "C:\Windows\Temp\Scripts"

New-Item -Force -Type "directory" $basedir | Out-Null

$au3_path = Join-Path $basedir vscode_minimize.au3

@'
#include <File.au3>; for TempFile(), _PathSplit(), And others
#include <Array.au3>
#include <MsgBoxConstants.au3>
#include <date.au3>
AutoItSetOption("MustDeclareVars", 1)

Global Const $LogDirectory = @WindowsDir & "\temp" & "\."
Global Const $log = $LogDirectory & "\" & @ScriptName & ".log"

Global $WinMatchMode
Global $WinMatchModeValue
Global $WinTitle
Global $WinEmbeddedText
Global $WinWaitTimeout

$WinTitle = "[REGEXPTITLE:.*Visual Studio Code.*]"
$WinWaitTimeout = 30
_FileWriteLog($log, StringFormat("INFO: LINE %04i: Starting %s from directory %s on machine %s", @ScriptLineNumber, @ScriptName, @ScriptDir, @ComputerName))
_FileWriteLog($log, StringFormat("INFO: LINE %04i: Waiting for window '%s'.  Will timeout after %s seconds", @ScriptLineNumber, $WinTitle, $WinWaitTimeout))
Local $found_window = WinWait($WinTitle, "", $WinWaitTimeout)
If Not $found_window Then
    _FileWriteLog($log, StringFormat("WARN: LINE %04i: I waited for %s seconds trying to find window with title '%s', but coundn't find it", @ScriptLineNumber, $WinWaitTimeout, $WinTitle))
EndIf
Local $aWinList = WinList($WinTitle)
Local $count = $aWinList[0][0]
_FileWriteLog($log, StringFormat("INFO: LINE %04i: I found %s window(s) matching title '%s'", @ScriptLineNumber, $count, $WinTitle))

For $i = 1 To Ubound($aWinList) - 1:
    Local $hWnd = $aWinList[$i][1]
    _FileWriteLog($log, StringFormat("INFO: LINE %04i: minimizing window with title matching '%s'", @ScriptLineNumber, $WinTitle))
    WinSetState($hWnd, "", @SW_MINIMIZE)
Next
'@ | Out-File -encoding ASCII $au3_path

$exe_path = Join-Path $basedir "$((Get-Item $au3_path).BaseName).exe"
$log_path = Join-Path $basedir "$((Get-Item $au3_path).BaseName).log"

Write-Host $au3_path
Write-Host $exe_path
Write-Host $log_path

$autoit = 'C:\Program Files (x86)\AutoIt3\autoit3.exe'
$autoit2exe = 'C:\Program Files (x86)\AutoIt3\Aut2Exe\aut2exe.exe'

$process = (Get-Item $au3_path).BaseName

Get-Process | Where-Object { $_.Name -eq "$process" } | Stop-Process

&$autoit2exe /in $au3_path /out $exe_path | Out-String

&$exe_path
