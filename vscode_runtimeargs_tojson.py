r"""
iconv -f utf-8 -t ascii//TRANSLIT
$env:APPDATA\Code\User\settings.json
{
    "telemetry.enableTelemetry": true,
}

$env:USERPROFILE\.vscode\argv.json
{
  "disable-color-correct-rendering": true,
  "enable-crash-reporter": true,
  "crash-reporter-id": "660cb7c7-a6f7-4476-ab8c-71efa9d5b491",
  "telemetry.enableTelemetry": "true"
}
"""

import argparse
import json
import pathlib
import re

parser = argparse.ArgumentParser()
parser.add_argument("path", help="please provide filename/filepath")
args = parser.parse_args()

settings_path = pathlib.Path(args.path)
out_path = settings_path.parent / f"{settings_path.stem}.1"

# turn $env:USERPROFILE\.vscode\argv.json into a real json file so I can edit it
lines = []
with open(settings_path) as infile:
    for line in infile:
        if mo := re.search(r"(.*?)//", line):
            keep = mo.group(1)
        else:
            keep = line

        if keep.strip():
            lines.append(keep)

with open(out_path, "w") as out:
    for line in lines:
        out.write(line)

with open(out_path) as fh:
    records = json.load(fh)
    records["enable-crash-reporter"] = "true"
    records["telemetry.enableTelemetry"] = "true"

settings_path.write_text(json.dumps(records, indent=2))
out_path.unlink()
