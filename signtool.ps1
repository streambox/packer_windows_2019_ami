choco install windows-sdk-10.1 --override-arguments --install-args="'/quiet /features Optionid.MSIInstallTools Optionid.SigningTools'"

$signtool = Get-ChildItem -Recurse "C:\Program*\Windows Kits\10\*\*\x64" -Filter "signtool.exe" | select-object -first 1

$CurrentValue = [Environment]::GetEnvironmentVariable("PATH", "User")
[Environment]::SetEnvironmentVariable("PATH", $CurrentValue + ";$($signtool.Directory.Fullname)", "User")
