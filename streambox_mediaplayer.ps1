Import-Module "$env:ChocolateyInstall\helpers\chocolateyInstaller.psm1" -Force

$url = [System.Uri]'http://update.streambox.com/updater/smphdwin/latest/StreamboxMPHD-Win.exe'
$packageName = Split-Path -Leaf -Path $url.AbsolutePath
$installer = Join-Path C:\Windows\Temp $packageName
$exe = $installer

try {
    Get-ChocolateyWebFile -FileFullPath $installer -PackageName $packageName -Url $url
} catch {}

&$installer /S

Get-Process | Where-Object { $_.Name -eq "StreamboxMPHD" } | Stop-Process
