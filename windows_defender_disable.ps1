<# -*- mode: org -*- #>

<# 

** start script

#+begin_example
#>

Import-Module ServerManager -Force
Import-Module "$env:ChocolateyInstall\helpers\chocolateyInstaller.psm1" -Force

Get-UACEnabled

$f = Get-WindowsFeature | Where-Object { $_.Name -eq "Windows-Defender" }
if($f){
    Remove-WindowsFeature Windows-Defender
}

$found = Get-ItemProperty -ErrorAction SilentlyContinue "HKLM:\SOFTWARE\Policies\Microsoft\Windows Defender" | Out-Null
If(!$found){
    Write-Output "Defender was successfully removed"
} else {
    Write-Warning "Defender was not removed, this is unexpected"
}

# this will be misleading until after you reboot:
# No: $f = Get-WindowsFeature | Where-Object { $_.Name -eq "Windows-Defender" }

exit 0

New-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows Defender" -Name DisableAntiSpyware -Value 1 -PropertyType DWORD -Force
if (Get-Command "Set-MpPreference" -ErrorAction SilentlyContinue) {
    Set-MpPreference -ErrorAction SilentlyContinue -ErrorVariable errs -DisableRealtimeMonitoring $true
    if (!$?) {
        if($errs[0].Exception.Message.Trim() -eq 'Invalid Class') {
            $errs.removeAt(0) # ignore invalid class errors
        } else {
            throw $errs[0].Exception
        }
    }
}

if ((Get-Command "Get-MpComputerStatus" -ErrorAction SilentlyContinue) -and (Get-MpComputerStatus -ErrorAction SilentlyContinue)) {
    Get-MpComputerStatus
}


$s = Get-Service | Where-Object { $_.DisplayName -eq "WinDefend" -or $_.Name -eq "WinDefend"}
if($s) {
    Stop-Service -Name WinDefend -Confirm:$false -Force
    Set-Service -Name WinDefend -StartupType Disabled
    Write-Output "stopped"
}

$regpath = "HKLM:\SOFTWARE\Policies\Microsoft\Windows Defender"
if (!(Test-Path $regpath -PathType Container)) {
    New-Item -Path $regpath -ItemType Container -Force
}
Set-ItemProperty -Path $regpath -Name DisableAntiSpyware -Value 1 -Type DWord -Force
$s = Get-Service | Where-Object { $_.DisplayName -eq "WinDefend" -or $_.Name -eq "WinDefend"}
if($s) {
    Stop-Service -Name WinDefend -Confirm:$false -Force
    Set-Service -Name WinDefend -StartupType Disabled
    Write-Output "stopped"
}

<#
#+end_example

** notes
*** error: Access is denied

https://ci.appveyor.com/project/TaylorMonacelli/packer-windows-2019-ami/builds/38289992?fullLog=true#L2788

How can "access denied" happen when I'm running under SYSTEM account?

**** googling

"access denied" SYSTEM account

**** log

#+begin_example
_server: RealTimeScanDirection           : 0
    amazon-ebs.windows_server: TamperProtectionSource          : N/A
    amazon-ebs.windows_server: PSComputerName                  :
    amazon-ebs.windows_server:
    amazon-ebs.windows_server: Stop-Service : Service 'Windows Defender Antivirus Service (WinDefend)' cannot be stopped due to the following error:
    amazon-ebs.windows_server: Cannot open WinDefend service on computer '.'.
    amazon-ebs.windows_server: At C:\Windows\Temp\script-60536416-5025-e8fd-46f3-87fc50fe5533.ps1:29 char:5
    amazon-ebs.windows_server: +     Stop-Service -Name WinDefend -Confirm:$false -Force
    amazon-ebs.windows_server: +     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    amazon-ebs.windows_server:     + CategoryInfo          : CloseError: (System.ServiceProcess.ServiceController:ServiceController) [Stop-Service],
    amazon-ebs.windows_server:    ServiceCommandException
    amazon-ebs.windows_server:     + FullyQualifiedErrorId : CouldNotStopService,Microsoft.PowerShell.Commands.StopServiceCommand
    amazon-ebs.windows_server:
    amazon-ebs.windows_server: Set-Service : Service 'Windows Defender Antivirus Service (WinDefend)' cannot be configured due to the following
    amazon-ebs.windows_server: error: Access is denied
    amazon-ebs.windows_server: At C:\Windows\Temp\script-60536416-5025-e8fd-46f3-87fc50fe5533.ps1:30 char:5
    amazon-ebs.windows_server: +     Set-Service -Name WinDefend -StartupType Disabled
    amazon-ebs.windows_server: +     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    amazon-ebs.windows_server:     + CategoryInfo          : PermissionDenied: (System.ServiceProcess.ServiceController:ServiceController) [Set-Servi
    amazon-ebs.windows_server:    ce], ServiceCommandException
    amazon-ebs.windows_server:     + FullyQualifiedErrorId : CouldNotSetService,Microsoft.PowerShell.Commands.SetServiceCommand
    amazon-ebs.windows_server:
    amazon-ebs.windows_server: stopped
    amazon-ebs.windows_server:
    amazon-ebs.windows_server: Property      : {}
    amazon-ebs.windows_server: PSPath        : Microsoft.PowerShell.Core\Registry::HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows Defender
    amazon-ebs.windows_server: PSParentPath  : Microsoft.PowerShell.Core\Registry::HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft
    amazon-ebs.windows_server: PSChildName   : Windows Defender
    amazon-ebs.windows_server: PSDrive       : HKLM
    amazon-ebs.windows_server: PSProvider    : Microsoft.PowerShell.Core\Registry
    amazon-ebs.windows_server: PSIsContainer : True
    amazon-ebs.windows_server: SubKeyCount   : 0
    amazon-ebs.windows_server: View          : Default
    amazon-ebs.windows_server: Handle        : Microsoft.Win32.SafeHandles.SafeRegistryHandle
    amazon-ebs.windows_server: ValueCount    : 0
    amazon-ebs.windows_server: Name          : HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows Defender
    amazon-ebs.windows_server:
    amazon-ebs.windows_server: Stop-Service : Service 'Windows Defender Antivirus Service (WinDefend)' cannot be stopped due to the following error:
    amazon-ebs.windows_server: Cannot open WinDefend service on computer '.'.
    amazon-ebs.windows_server: At C:\Windows\Temp\script-60536416-5025-e8fd-46f3-87fc50fe5533.ps1:41 char:5
    amazon-ebs.windows_server: +     Stop-Service -Name WinDefend -Confirm:$false -Force
    amazon-ebs.windows_server: +     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    amazon-ebs.windows_server:     + CategoryInfo          : CloseError: (System.ServiceProcess.ServiceController:ServiceController) [Stop-Service],
    amazon-ebs.windows_server:    ServiceCommandException
    amazon-ebs.windows_server:     + FullyQualifiedErrorId : CouldNotStopService,Microsoft.PowerShell.Commands.StopServiceCommand
    amazon-ebs.windows_server:
    amazon-ebs.windows_server: Set-Service : Service 'Windows Defender Antivirus Service (WinDefend)' cannot be configured due to the following
    amazon-ebs.windows_server: error: Access is denied
    amazon-ebs.windows_server: At C:\Windows\Temp\script-60536416-5025-e8fd-46f3-87fc50fe5533.ps1:42 char:5
    amazon-ebs.windows_server: +     Set-Service -Name WinDefend -StartupType Disabled
    amazon-ebs.windows_server: +     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    amazon-ebs.windows_server:     + CategoryInfo          : PermissionDenied: (System.ServiceProcess.ServiceController:ServiceController) [Set-Servi
    amazon-ebs.windows_server:    ce], ServiceCommandException
    amazon-ebs.windows_server:     + FullyQualifiedErrorId : CouldNotSetService,Microsoft.PowerShell.Commands.SetServiceCommand
    amazon-ebs.windows_server:
    amazon-ebs.windows_server: stopped
==> amazon-ebs.windows_server: Uploading vscode_runtimeargs_tojson.py => C:\Windows\Temp\scripts\vscode_runtimeargs_tojson.py
    amazon-ebs.windows_server: vscode_runtimeargs_tojson.py 1.23 KiB / 1.23 KiB [=============================================================================================================================================] 100.00% 1s
==> amazon-ebs.windows_server: Uploading resources/iconfinder_exit_17902.ico => C:\Windows\Temp\iconfinder_exit_17902.ico
    amazon-ebs.windows_server: iconfinder_exit_17902.ico 141.37 KiB / 141.37 KiB [============================================================================================================================================] 100.00% 4s
==> amazon-ebs.windows_server: Provisioning with Powershell...
==> amazon-ebs.windows_server: Provisioning with powershell script: ./generated/taskbar_customize.ps1
    amazon-ebs.windows_server: Transcript started, output file is C:\Windows\Temp\logs_EC2AMAZ-J8I8FIB$\script-60536463-cdba-925a-6bfa-d2cdd6d96d0f.log
    amazon-ebs.windows_server:
    amazon-ebs.windows_server:
    amazon-ebs.windows_server:     Hive: HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer
    amazon-ebs.windows_server:
    amazon-ebs.windows_server:
    amazon-ebs.windows_server: Name                           Property
    amazon-ebs.windows_server: ----                           --------
    amazon-ebs.windows_server: Advanced
==> amazon-ebs.windows_server: Provisioning with Powershell...
==> amazon-ebs.windows_server: Provisioning with powershell script: /tmp/powershell-provisioner673772512
    amazon-ebs.windows_server:
    amazon-ebs.windows_server: TaskPath                                       TaskName                          State
    amazon-ebs.windows_server: --------                                       --------                          -----
    amazon-ebs.windows_server: \                                              Amazon Ec2 Launch - Windows is... Ready
==> amazon-ebs.windows_server: Provisioning with Powershell...
==> amazon-ebs.windows_server: Provisioning with powershell script: /tmp/powershell-provisioner723378619
    amazon-ebs.windows_server:
    amazon-ebs.windows_server: TaskPath                                       TaskName                          State
    amazon-ebs.windows_server: --------                                       --------                          -----
    amazon-ebs.windows_server: \                                              Amazon Ec2 Launch - Instance I... Ready
==> amazon-ebs.windows_server: Stopping the source instance...
    amazon-ebs.windows_server: Stopping instance
==> amazon-ebs.windows_server: Waiting for the instance to stop...
==> amazon-ebs.windows_server: Deregistered AMI windows-example, id: ami-0ccbe92d0c9ccb2f5
==> amazon-ebs.windows_server: Deleted snapshot: snap-0ad8ef417999a8f72
==> amazon-ebs.windows_server: Creating AMI windows-example from instance i-0de7fba7d69396eaf
    amazon-ebs.windows_server: AMI: ami-05b286e9209d3f442
==> amazon-ebs.windows_server: Waiting for AMI to become ready...
#+end_example

*** 

#+begin_example
Write-Host "Disabling Windows Defender..."
New-Item -Force -Path 'HKLM:\Software\Policies\Microsoft\Windows Defender'
Set-ItemProperty -Path "HKLM:\Software\Policies\Microsoft\Windows Defender" -Name "DisableAntiSpyware" -Type DWord -Value 1

Get-Service | Where-Object { $_.Name -eq "WdNisSvc" } | Stop-Service
# Get-Service | Where-Object { $_.Name -eq "mpssvc" } | Stop-Service
Get-Service | Where-Object { $_.Name -eq "WinDefend" } | Stop-Service
#+end_example

*** 

#+begin_example
Get-Service | Out-String -Stream | sls def

PS C:\> Get-Service | Out-String -Stream | sls def

Stopped  defragsvc          Optimize drives
Running  mpssvc             Windows Defender Firewall
Stopped  Sense              Windows Defender Advanced Threat Pr...
Running  WdNisSvc           Windows Defender Antivirus Network ...
Running  WinDefend          Windows Defender Antivirus Service
PS C:\>
#+end_example

** detect if defender was removed properly

#+begin_example
$found = Get-ItemProperty -ErrorAction SilentlyContinue "HKLM:\SOFTWARE\Policies\Microsoft\Windows Defender" | Out-Null
if(!$found){
    Write-Output "Defender was successfully removed"
} else {
    Write-Warning "Defender was not removed, this is unexpected"
}
#+end_example

** end notes

#>
