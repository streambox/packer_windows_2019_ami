$secrets_path = "C:\\ProgramData\\secrets.json"

$credentials = Get-Content -Raw -Path $secrets_path | ConvertFrom-Json

$user = Get-LocalUser -ErrorAction SilentlyContinue $credentials.username1

$pass = ConvertTo-SecureString -AsPlainText -Force $credentials.password1
if(!$user){
    $user = New-LocalUser -Password $pass $credentials.username1
}
$user | Set-LocalUser -Password $pass

$inGroup = Get-LocalGroupMember -ErrorAction SilentlyContinue -Member $credentials.username1 -Group Administrators
if(!$inGroup) {
    Add-LocalGroupMember -Group Administrators -Member $credentials.username1
}
Set-LocalUser -Name $credentials.username1 -PasswordNeverExpires 1

Remove-Item -ErrorAction SilentlyContinue -Force $secrets_path | Out-Null
