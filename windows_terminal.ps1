<#
https://chocolatey.org/packages/microsoft-windows-terminal

This package requires at least Windows 10 version 1903/OS build 18362.x.
#>


If([System.Environment]::OSVersion.Version -lt [version]"10.1903.18362.0") {
    exit 0
}

choco install microsoft-windows-terminal
