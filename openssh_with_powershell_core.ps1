New-ItemProperty -Path "HKLM:\SOFTWARE\OpenSSH" -Name DefaultShell -Value "C:\Program Files\PowerShell\7\pwsh.exe" -PropertyType String -Force

exit 0

# https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_server_configuration
# New-ItemProperty -Path "HKLM:\SOFTWARE\OpenSSH" -Name DefaultShell -Value "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" -PropertyType String -Force
# https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_pwsh?view=powershell-7.1

<#

** 

Warning: This workaround breaks ansible

workaround:
https://github.com/PowerShell/Win32-OpenSSH/issues/1677

#>

@'
@echo off

pwsh -NoLogo
'@ | Out-File -Encoding ASCII 'C:\Program Files\PowerShell\7\pwsh.bat'
New-ItemProperty -Path "HKLM:\SOFTWARE\OpenSSH" -Name DefaultShell -Value "C:\Program Files\PowerShell\7\pwsh.bat" -PropertyType String -Force



New-Item -Type "directory" -Force C:\ProgramData\ssh | Out-Null

<#

FIXME: the following doesn't get rid of the prompt:
PowerShell 7.1.2
Copyright (c) Microsoft Corporation.

https://aka.ms/powershell
Type 'help' to get help.

PS C:\Users\Administrator>

https://github.com/PowerShell/Win32-OpenSSH/issues/1677
pwsh DefaultShellCommandOption

#>
New-ItemProperty -Path "HKLM:\SOFTWARE\OpenSSH" -Name DefaultShellCommandOption -Value "-NoLogo" -PropertyType String -Force
(Get-ItemProperty HKLM:\SOFTWARE\OpenSSH).DefaultShellCommandOption

<#

powershell count lines in file
gci -r c:\ -ea 0 | select -exp fullname >>c:\all.txt
Get-Content C:\all.txt | Measure-Object -Line
cat c:\all.txt | sls SSHDefaultShell
ls "C:\Program Files\openssh-win64"
C:\Program Files\openssh-win64\Set-SSHDefaultShell.ps1

#>
