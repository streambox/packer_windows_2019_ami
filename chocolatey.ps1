# Set-PSDebug -Trace 1

If(Get-Command -ErrorAction SilentlyContinue choco) {
    exit
}

Set-ExecutionPolicy Bypass -Scope Process -Force

Invoke-WebRequest -OutFile C:\Windows\Temp\chocolatey_install.ps1 'https://chocolatey.org/install.ps1'
C:\Windows\Temp\chocolatey_install.ps1

# ummm, --yes, its what I mean, thank you
choco feature enable -n allowGlobalConfirmation

# don't clutter appveyor logs please
choco feature disable --name showDownloadProgress
choco feature disable --name showNonElevatedWarnings
choco feature enable --name usePackageExitCodes
choco feature enable --name useEnhancedExitCodes
choco feature enable --name stopOnFirstPackageFailure
