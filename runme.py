import pathlib
import textwrap
import shutil
import jinja2

tmp = pathlib.Path("generated")
hcl = tmp / "stuff.hcl"
hcl2 = tmp / "win.pkr.hcl"
runonce = tmp / pathlib.Path("runonce_customize.ps1")

if tmp.exists():
    shutil.rmtree(tmp)

tmp.mkdir(parents=True, exist_ok=False)

count = 0
for path in pathlib.Path(".").glob("*customize*.ps1.*"):
    if path.name.startswith('.#') or path.name.startswith('#'):
        continue

    basename = path.stem.replace('.tmpl', '').replace('.ps1', '')
    vbs = tmp / f"{basename}.vbs"

    count += 1
    tpl = textwrap.dedent(
        f"""
    Dim shell,command
    command = "C:\\WINDOWS\\system32\\WindowsPowerShell\\v1.0\\powershell.exe -NoLogo -File C:\\Windows\\Temp\\scripts\\{path.stem.replace('.tmpl', '')}"
    Set shell = CreateObject("WScript.Shell")
    shell.Run command,0
    """
    ).strip()

    vbs.write_text(f"{tpl}\n")

    tpl2 = textwrap.dedent(
        f"""
    Set-ItemProperty "HKLM:\Software\Microsoft\Windows\CurrentVersion\RunOnce" -Name '!customize{count:>03d}' -Value "C:\Windows\system32\wscript.exe C:\Windows\Temp\scripts\{vbs.name}"
    """
    ).strip()

    # add logging
    text = path.read_text()
    logging_preamble = textwrap.dedent("""
    $base = (Get-Item $MyInvocation.MyCommand.Path).Basename
    $dir = Split-Path -Parent -Path $MyInvocation.MyCommand.Path
    $log_path = Join-Path $dir "logs_$($env:USERNAME)" | Join-Path -ChildPath "$($base).log"
    New-Item -Force -Type "directory" (Split-Path -Parent -Path $log_path) | Out-Null
    Start-Transcript -Path $log_path

    # Set-PSdebug -Trace 1
    """).strip()
    text = text.replace('{{start_transcript}}', logging_preamble)
    script = tmp / path.name.replace('.tmpl', '')
    script.write_text(text)

    with runonce.open("a") as f:
        f.write(f"{tpl2}\n")

    x = tmp / path.name.replace('.tmpl', '')

    tpl3 = textwrap.dedent(
        f"""
    provisioner "file" {{
      source = "{x}"
      destination = "C:\\\\Windows\\\\Temp\\\\scripts\\\\{x.name}"
    }}
    provisioner "file" {{
      source = "{vbs}"
      destination = "C:\\\\Windows\\\\Temp\\\\scripts\\\\{vbs.name}"
    }}
    """
    ).lstrip()

    with hcl.open("a") as f:
        f.write(f"{tpl3}\n")

stuff = hcl.read_text()

templateLoader = jinja2.FileSystemLoader(searchpath="./")
templateEnv = jinja2.Environment(loader=templateLoader)
TEMPLATE_FILE = "win.pkr.hcl.j2"
template = templateEnv.get_template(TEMPLATE_FILE)
outputText = template.render(file_provisioner=stuff)
open(hcl2, "w").write(outputText)
