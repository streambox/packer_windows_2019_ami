$base = (Get-Item $MyInvocation.MyCommand.Path).Basename
$dir = Split-Path -Parent -Path $MyInvocation.MyCommand.Path
$log_path = Join-Path $dir "logs_$($env:USERNAME)" | Join-Path -ChildPath "$($base).log"
New-Item -Force -Type "directory" (Split-Path -Parent -Path $log_path) | Out-Null
Start-Transcript -Path $log_path

# Set-PSdebug -Trace 1

code --list-extensions
code --install-extension ms-vscode.powershell
code --list-extensions

<#

* start

** try restarting language server by using trash can from here

https://github.com/PowerShell/vscode-powershell/issues/3227#issuecomment-793192452

** 

This seems to be same as my problem

https://github.com/PowerShell/vscode-powershell/issues/3227
https://github.com/PowerShell/vscode-powershell/issues/3227

but for some reason that ticket is closed, but i've not delved too deep yet...

** 

I give up trying...

Heres failing log


#+begin_example
3/9/2021 9:57:43 PM [NORMAL] - Visual Studio Code v1.54.1 64-bit
3/9/2021 9:57:43 PM [NORMAL] - PowerShell Extension v2021.2.2
3/9/2021 9:57:43 PM [NORMAL] - Operating System: Windows 64-bit
3/9/2021 9:57:43 PM [NORMAL] - Language server starting --
3/9/2021 9:57:43 PM [NORMAL] -     PowerShell executable: C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe
3/9/2021 9:57:43 PM [NORMAL] -     PowerShell args: -NoProfile -NonInteractive -ExecutionPolicy Bypass -Command Import-Module 'c:\Users\taylor\.vscode\extensions\ms-vscode.powershell-2021.2.2\modules\PowerShellEditorServices\PowerShellEditorServices.psd1'; Start-EditorServices -HostName 'Visual Studio Code Host' -HostProfileId 'Microsoft.VSCode' -HostVersion '2021.2.2' -AdditionalModules @('PowerShellEditorServices.VSCode') -BundledModulesPath 'c:\Users\taylor\.vscode\extensions\ms-vscode.powershell-2021.2.2\modules' -EnableConsoleRepl -StartupBanner '=====> PowerShell Integrated Console v2021.2.2 <=====
' -LogLevel 'Normal' -LogPath 'c:\Users\taylor\.vscode\extensions\ms-vscode.powershell-2021.2.2\logs\1615327063-2fad13fc-1164-420f-8369-e845c68a67e01615327060106\EditorServices.log' -SessionDetailsPath 'c:\Users\taylor\.vscode\extensions\ms-vscode.powershell-2021.2.2\sessions\PSES-VSCode-2584-125565' -FeatureFlags @()
3/9/2021 9:57:43 PM [NORMAL] -     PowerShell Editor Services args: Import-Module 'c:\Users\taylor\.vscode\extensions\ms-vscode.powershell-2021.2.2\modules\PowerShellEditorServices\PowerShellEditorServices.psd1'; Start-EditorServices -HostName 'Visual Studio Code Host' -HostProfileId 'Microsoft.VSCode' -HostVersion '2021.2.2' -AdditionalModules @('PowerShellEditorServices.VSCode') -BundledModulesPath 'c:\Users\taylor\.vscode\extensions\ms-vscode.powershell-2021.2.2\modules' -EnableConsoleRepl -StartupBanner '=====> PowerShell Integrated Console v2021.2.2 <=====
' -LogLevel 'Normal' -LogPath 'c:\Users\taylor\.vscode\extensions\ms-vscode.powershell-2021.2.2\logs\1615327063-2fad13fc-1164-420f-8369-e845c68a67e01615327060106\EditorServices.log' -SessionDetailsPath 'c:\Users\taylor\.vscode\extensions\ms-vscode.powershell-2021.2.2\sessions\PSES-VSCode-2584-125565' -FeatureFlags @()
3/9/2021 9:57:43 PM [NORMAL] - powershell.exe started.
3/9/2021 9:57:43 PM [NORMAL] - Waiting for session file
3/9/2021 9:57:45 PM [NORMAL] - Session file found
3/9/2021 9:57:45 PM [NORMAL] - Registering terminal close callback
3/9/2021 9:57:45 PM [NORMAL] - Registering terminal PID log callback
3/9/2021 9:57:45 PM [NORMAL] - powershell.exe PID: 7664
3/9/2021 9:57:45 PM [NORMAL] - Language server started.
3/9/2021 9:57:45 PM [NORMAL] - {"status":"started","languageServiceTransport":"NamedPipe","languageServicePipeName":"\\\\.\\pipe\\PSES_zfqn21io.bei","debugServiceTransport":"NamedPipe","debugServicePipeName":"\\\\.\\pipe\\PSES_lbv0mlj1.fih"}
3/9/2021 9:57:45 PM [NORMAL] - Connecting to language service on pipe \\.\pipe\PSES_zfqn21io.bei...
3/9/2021 9:57:45 PM [NORMAL] - Language service connected.
3/9/2021 9:57:45 PM [ERROR] - Could not start language service:
3/9/2021 9:57:45 PM [ERROR] - Error: Connection to server got closed. Server will not be restarted.
#+end_example

** 

I tried uninstalling pwsh 7.1.2 hoping maybe vscode would then drop
trying to use pwsh.exe under the assumption pwssh.exe was causing
the problem.

but still the problem remiains using:
C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe

#+begin_example
3/9/2021 9:42:32 PM [NORMAL] - Visual Studio Code v1.54.1 64-bit
3/9/2021 9:42:32 PM [NORMAL] - PowerShell Extension v2021.2.2
3/9/2021 9:42:32 PM [NORMAL] - Operating System: Windows 64-bit
3/9/2021 9:42:32 PM [NORMAL] - Language server starting --
3/9/2021 9:42:32 PM [NORMAL] -     PowerShell executable: C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe
3/9/2021 9:42:32 PM [NORMAL] -     PowerShell args: -NoProfile -NonInteractive -ExecutionPolicy Bypass -Command Import-Module 'c:\Users\taylor\.vscode\extensions\ms-vscode.powershell-2021.2.2\modules\PowerShellEditorServices\PowerShellEditorServices.psd1'; Start-EditorServices -HostName 'Visual Studio Code Host' -HostProfileId 'Microsoft.VSCode' -HostVersion '2021.2.2' -AdditionalModules @('PowerShellEditorServices.VSCode') -BundledModulesPath 'c:\Users\taylor\.vscode\extensions\ms-vscode.powershell-2021.2.2\modules' -EnableConsoleRepl -StartupBanner '=====> PowerShell Integrated Console v2021.2.2 <=====
' -LogLevel 'Normal' -LogPath 'c:\Users\taylor\.vscode\extensions\ms-vscode.powershell-2021.2.2\logs\1615326152-4368539f-f514-4e44-a1b3-041a40b4a17c1615326149098\EditorServices.log' -SessionDetailsPath 'c:\Users\taylor\.vscode\extensions\ms-vscode.powershell-2021.2.2\sessions\PSES-VSCode-4592-425921' -FeatureFlags @()
3/9/2021 9:42:32 PM [NORMAL] -     PowerShell Editor Services args: Import-Module 'c:\Users\taylor\.vscode\extensions\ms-vscode.powershell-2021.2.2\modules\PowerShellEditorServices\PowerShellEditorServices.psd1'; Start-EditorServices -HostName 'Visual Studio Code Host' -HostProfileId 'Microsoft.VSCode' -HostVersion '2021.2.2' -AdditionalModules @('PowerShellEditorServices.VSCode') -BundledModulesPath 'c:\Users\taylor\.vscode\extensions\ms-vscode.powershell-2021.2.2\modules' -EnableConsoleRepl -StartupBanner '=====> PowerShell Integrated Console v2021.2.2 <=====
' -LogLevel 'Normal' -LogPath 'c:\Users\taylor\.vscode\extensions\ms-vscode.powershell-2021.2.2\logs\1615326152-4368539f-f514-4e44-a1b3-041a40b4a17c1615326149098\EditorServices.log' -SessionDetailsPath 'c:\Users\taylor\.vscode\extensions\ms-vscode.powershell-2021.2.2\sessions\PSES-VSCode-4592-425921' -FeatureFlags @()
3/9/2021 9:42:32 PM [NORMAL] - powershell.exe started.
3/9/2021 9:42:32 PM [NORMAL] - Waiting for session file
3/9/2021 9:42:34 PM [NORMAL] - Session file found
3/9/2021 9:42:34 PM [NORMAL] - Registering terminal close callback
3/9/2021 9:42:34 PM [NORMAL] - Registering terminal PID log callback
3/9/2021 9:42:34 PM [NORMAL] - powershell.exe PID: 4308
3/9/2021 9:42:34 PM [NORMAL] - Language server started.
3/9/2021 9:42:34 PM [NORMAL] - {"status":"started","languageServiceTransport":"NamedPipe","languageServicePipeName":"\\\\.\\pipe\\PSES_2h2dyszi.b4t","debugServiceTransport":"NamedPipe","debugServicePipeName":"\\\\.\\pipe\\PSES_cuwh3jqe.osu"}
3/9/2021 9:42:34 PM [NORMAL] - Connecting to language service on pipe \\.\pipe\PSES_2h2dyszi.b4t...
3/9/2021 9:42:34 PM [NORMAL] - Language service connected.
3/9/2021 9:42:35 PM [ERROR] - Could not start language service:
3/9/2021 9:42:35 PM [ERROR] - Error: Connection to server got closed. Server will not be restarted.
#+end_example

** 

C:\Users\taylor\AppData\Roaming\Code\User\Settings.json

#+begin_example
{
    "powershell.powerShellDefaultVersion": "C:/WINDOWS/system32/WindowsPowerShell/v1.0/powershell.exe"
}
#+end_example

#+begin_example
{
    "powershell.powerShellDefaultVersion": "C:/WINDOWS/system32/WindowsPowerShell/v1.0/powershell.exe",
    "powershell.powerShellAdditionalExePaths": [

    ]
}
#+end_example

** 

powerShellDefaultVersion
"powershell.powerShellDefaultVersion": "C:/WINDOWS/system32/WindowsPowerShell/v1.0/powershell.exe"

** 

powerShellDefaultVersion
vscode powerShellDefaultVersion
the "powershell.powerShellDefaultVersion" se

** 

"powershell.powerShellExePath": "c:/Program Files/PowerShell/<version>/pwsh.exe"
C:\WINDOWS\system32\WindowsPowerShell\v1.0\powershell.exe
C:/WINDOWS/system32/WindowsPowerShell/v1.0/powershell.exe

vscode powerShellExePath
"powershell.powerShellExePath": "C:/WINDOWS/system32/WindowsPowerShell/v1.0/powershell.exe"

** 

vscode uses pwsh

** 

https://github.com/microsoft/vscode/issues/117649

** googling 

PowerShell executable pwsh.exe Error: Connection to server got closed. Server will not be restarted. Could not start language service:

PowerShell executable pwsh.exe
Error: Connection to server got closed. Server will not be restarted. Could not start language service:

** log 

#+begin_example
3/9/2021 9:27:06 PM [NORMAL] - Visual Studio Code v1.54.1 64-bit
3/9/2021 9:27:06 PM [NORMAL] - PowerShell Extension v2021.2.2
3/9/2021 9:27:06 PM [NORMAL] - Operating System: Windows 64-bit
3/9/2021 9:27:06 PM [NORMAL] - Language server starting --
3/9/2021 9:27:06 PM [NORMAL] -     PowerShell executable: C:\Program Files\PowerShell\7\pwsh.exe
3/9/2021 9:27:06 PM [NORMAL] -     PowerShell args: -NoProfile -NonInteractive -ExecutionPolicy Bypass -Command Import-Module 'c:\Users\taylor\.vscode\extensions\ms-vscode.powershell-2021.2.2\modules\PowerShellEditorServices\PowerShellEditorServices.psd1'; Start-EditorServices -HostName 'Visual Studio Code Host' -HostProfileId 'Microsoft.VSCode' -HostVersion '2021.2.2' -AdditionalModules @('PowerShellEditorServices.VSCode') -BundledModulesPath 'c:\Users\taylor\.vscode\extensions\ms-vscode.powershell-2021.2.2\modules' -EnableConsoleRepl -StartupBanner '=====> PowerShell Integrated Console v2021.2.2 <=====
' -LogLevel 'Normal' -LogPath 'c:\Users\taylor\.vscode\extensions\ms-vscode.powershell-2021.2.2\logs\1615325226-83a61fd9-69dd-4204-866b-061401841d0e1615325224107\EditorServices.log' -SessionDetailsPath 'c:\Users\taylor\.vscode\extensions\ms-vscode.powershell-2021.2.2\sessions\PSES-VSCode-5828-281078' -FeatureFlags @()
3/9/2021 9:27:06 PM [NORMAL] -     PowerShell Editor Services args: Import-Module 'c:\Users\taylor\.vscode\extensions\ms-vscode.powershell-2021.2.2\modules\PowerShellEditorServices\PowerShellEditorServices.psd1'; Start-EditorServices -HostName 'Visual Studio Code Host' -HostProfileId 'Microsoft.VSCode' -HostVersion '2021.2.2' -AdditionalModules @('PowerShellEditorServices.VSCode') -BundledModulesPath 'c:\Users\taylor\.vscode\extensions\ms-vscode.powershell-2021.2.2\modules' -EnableConsoleRepl -StartupBanner '=====> PowerShell Integrated Console v2021.2.2 <=====
' -LogLevel 'Normal' -LogPath 'c:\Users\taylor\.vscode\extensions\ms-vscode.powershell-2021.2.2\logs\1615325226-83a61fd9-69dd-4204-866b-061401841d0e1615325224107\EditorServices.log' -SessionDetailsPath 'c:\Users\taylor\.vscode\extensions\ms-vscode.powershell-2021.2.2\sessions\PSES-VSCode-5828-281078' -FeatureFlags @()
3/9/2021 9:27:06 PM [NORMAL] - pwsh.exe started.
3/9/2021 9:27:06 PM [NORMAL] - Waiting for session file
3/9/2021 9:27:08 PM [NORMAL] - Session file found
3/9/2021 9:27:08 PM [NORMAL] - Registering terminal close callback
3/9/2021 9:27:08 PM [NORMAL] - Registering terminal PID log callback
3/9/2021 9:27:08 PM [NORMAL] - pwsh.exe PID: 1692
3/9/2021 9:27:08 PM [NORMAL] - Language server started.
3/9/2021 9:27:08 PM [NORMAL] - {"status":"started","languageServiceTransport":"NamedPipe","languageServicePipeName":"\\\\.\\pipe\\PSES_otv4kmrc.thr","debugServiceTransport":"NamedPipe","debugServicePipeName":"\\\\.\\pipe\\PSES_zwgyumvj.2tj"}
3/9/2021 9:27:08 PM [NORMAL] - Connecting to language service on pipe \\.\pipe\PSES_otv4kmrc.thr...
3/9/2021 9:27:08 PM [NORMAL] - Language service connected.
3/9/2021 9:27:09 PM [ERROR] - Could not start language service:
3/9/2021 9:27:09 PM [ERROR] - Error: Connection to server got closed. Server will not be restarted.
#+end_example

*

code --list-extensions

* code:

#>


