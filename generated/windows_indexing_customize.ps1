<#

windows turn on indexing service

wsearch

changes:

FIXME: putting start_transcript block inside backgound block didnt'
seem to work...testing further

#>

$block = {
  Get-Service -Name WSearch
  Set-Service -Name WSearch -StartupType Automatic
  Start-Service -Name WSearch
  Get-Service -Name WSearch
}

$base = (Get-Item $MyInvocation.MyCommand.Path).Basename
$dir = Split-Path -Parent -Path $MyInvocation.MyCommand.Path
$log_path = Join-Path $dir "logs_$($env:USERNAME)" | Join-Path -ChildPath "$($base).log"
New-Item -Force -Type "directory" (Split-Path -Parent -Path $log_path) | Out-Null
Start-Transcript -Path $log_path

# Set-PSdebug -Trace 1

Start-Job -Name windows_indexing_customize -Scriptblock $block
