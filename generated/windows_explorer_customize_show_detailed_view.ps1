<#

https://www.tenforums.com/customization/143079-universal-reg-edit-every-folder-default-detailed-view.html

Windows Registry Editor Version 5.00

; First delete the views that have already been stored (the keys will be re-created automatically)
[-HKEY_CURRENT_USER\Software\Microsoft\Windows\Shell\Bags]
[-HKEY_CURRENT_USER\Software\Microsoft\Windows\Shell\BagMRU]

; Now change the record store to Mode 4 (Details)
[HKEY_CURRENT_USER\Software\Microsoft\Windows\Shell\Bags\AllFolders\Shell]
"WFlags"=dword:00000000
"Status"=dword:00000000
"Mode"=dword:00000004
"vid"="{137E7700-3573-11CF-AE69-08002B2E1262}"

#>


<#

windows explorer show detailed view powershell
Get-ChildItem
Get-ChildItem windows explorer show detailed view
Set-ItemProperty LogicalViewMode details view


https://superuser.com/questions/1498668/how-do-you-default-the-windows-10-explorer-view-to-details-when-looking-at-sea/1499413

(gci 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FolderTypes' |
   ?{(gp $_.PSPath).CanonicalName -match '\.S'}).PSChildname | %{
         $SRPath = "HKCU:\Software\Classes\Local Settings\Software\Microsoft\Windows\Shell\Bags\AllFolders\Shell\$_"
         new-item -Path $SRPath
         New-ItemProperty -Path $SRPath -Name 'Mode' -Value 4
      }

#>

# https://superuser.com/a/1575806/185226

$base = (Get-Item $MyInvocation.MyCommand.Path).Basename
$dir = Split-Path -Parent -Path $MyInvocation.MyCommand.Path
$log_path = Join-Path $dir "logs_$($env:USERNAME)" | Join-Path -ChildPath "$($base).log"
New-Item -Force -Type "directory" (Split-Path -Parent -Path $log_path) | Out-Null
Start-Transcript -Path $log_path

# Set-PSdebug -Trace 1

$FT = 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FolderTypes'
Get-ChildItem $FT -Recurse | ? Property -Contains 'LogicalVIewMode' | %{
    $_.Name -match 'FolderTypes\\(.+)\\TopViews' | out-null
    $matches[1] } | select -unique | %{
        New-Item "HKLM:\SOFTWARE\Microsoft\Windows\Shell\Bags\AllFolders\Shell\$_" -force |
        Set-ItemProperty -Name 'Mode' -Value 3 -PassThru |
        Set-ItemProperty -Name 'LogicalViewMode' -Value 4
}
