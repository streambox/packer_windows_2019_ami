$base = (Get-Item $MyInvocation.MyCommand.Path).Basename
$dir = Split-Path -Parent -Path $MyInvocation.MyCommand.Path
$log_path = Join-Path $dir "logs_$($env:USERNAME)" | Join-Path -ChildPath "$($base).log"
New-Item -Force -Type "directory" (Split-Path -Parent -Path $log_path) | Out-Null
Start-Transcript -Path $log_path

# Set-PSdebug -Trace 1

# https://www.tenforums.com/tutorials/6815-set-network-location-private-public-domain-windows-10-a.html#option4
# https://www.google.com/search?q=%22powershell%22+do+you+want+to+allow+your+pc+to+be+discoverable&client=emacs&sxsrf=ALeKk01Wiuh1PmNgkghXsBd67KOBxh-Wvw:1614448522535&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjOisa10YrvAhVCtZ4KHTEuBjEQ_AUoA3oECA8QBQ&biw=1873&bih=1018#imgrc=1zkdqSmU2vo4_M
# "powershell" do you want to allow your pc to be discoverable

foreach ($profile in Get-NetConnectionProfile) {
    Set-NetConnectionProfile -Name $profile.name -NetworkCategory Public
}
