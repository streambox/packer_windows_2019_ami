provisioner "file" {
  source = "generated/psgallery_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\psgallery_customize.ps1"
}
provisioner "file" {
  source = "generated/psgallery_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\psgallery_customize.vbs"
}

provisioner "file" {
  source = "generated/windows_explorer_customize_stop_windows_explorer.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\windows_explorer_customize_stop_windows_explorer.ps1"
}
provisioner "file" {
  source = "generated/windows_explorer_customize_stop_windows_explorer.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\windows_explorer_customize_stop_windows_explorer.vbs"
}

provisioner "file" {
  source = "generated/vscode_default_settings_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\vscode_default_settings_customize.ps1"
}
provisioner "file" {
  source = "generated/vscode_default_settings_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\vscode_default_settings_customize.vbs"
}

provisioner "file" {
  source = "generated/powershell_core_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\powershell_core_customize.ps1"
}
provisioner "file" {
  source = "generated/powershell_core_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\powershell_core_customize.vbs"
}

provisioner "file" {
  source = "generated/golang_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\golang_customize.ps1"
}
provisioner "file" {
  source = "generated/golang_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\golang_customize.vbs"
}

provisioner "file" {
  source = "generated/vscode_powershell_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\vscode_powershell_customize.ps1"
}
provisioner "file" {
  source = "generated/vscode_powershell_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\vscode_powershell_customize.vbs"
}

provisioner "file" {
  source = "generated/git_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\git_customize.ps1"
}
provisioner "file" {
  source = "generated/git_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\git_customize.vbs"
}

provisioner "file" {
  source = "generated/powershell_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\powershell_customize.ps1"
}
provisioner "file" {
  source = "generated/powershell_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\powershell_customize.vbs"
}

provisioner "file" {
  source = "generated/powershell_customize_windows.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\powershell_customize_windows.ps1"
}
provisioner "file" {
  source = "generated/powershell_customize_windows.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\powershell_customize_windows.vbs"
}

provisioner "file" {
  source = "generated/customize_during_first_login.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\customize_during_first_login.ps1"
}
provisioner "file" {
  source = "generated/customize_during_first_login.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\customize_during_first_login.vbs"
}

provisioner "file" {
  source = "generated/powershell_customize_profile.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\powershell_customize_profile.ps1"
}
provisioner "file" {
  source = "generated/powershell_customize_profile.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\powershell_customize_profile.vbs"
}

provisioner "file" {
  source = "generated/vscode_customize_add_vim_extension.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\vscode_customize_add_vim_extension.ps1"
}
provisioner "file" {
  source = "generated/vscode_customize_add_vim_extension.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\vscode_customize_add_vim_extension.vbs"
}

provisioner "file" {
  source = "generated/google_chrome_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\google_chrome_customize.ps1"
}
provisioner "file" {
  source = "generated/google_chrome_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\google_chrome_customize.vbs"
}

provisioner "file" {
  source = "generated/desktop_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\desktop_customize.ps1"
}
provisioner "file" {
  source = "generated/desktop_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\desktop_customize.vbs"
}

provisioner "file" {
  source = "generated/windows_explorer_customize_show_detailed_view.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\windows_explorer_customize_show_detailed_view.ps1"
}
provisioner "file" {
  source = "generated/windows_explorer_customize_show_detailed_view.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\windows_explorer_customize_show_detailed_view.vbs"
}

provisioner "file" {
  source = "generated/taskbar_customize_add_shutdown_shortcut_to_taskbar.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\taskbar_customize_add_shutdown_shortcut_to_taskbar.ps1"
}
provisioner "file" {
  source = "generated/taskbar_customize_add_shutdown_shortcut_to_taskbar.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\taskbar_customize_add_shutdown_shortcut_to_taskbar.vbs"
}

provisioner "file" {
  source = "generated/powershell_customize_macos.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\powershell_customize_macos.ps1"
}
provisioner "file" {
  source = "generated/powershell_customize_macos.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\powershell_customize_macos.vbs"
}

provisioner "file" {
  source = "generated/taskbar_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\taskbar_customize.ps1"
}
provisioner "file" {
  source = "generated/taskbar_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\taskbar_customize.vbs"
}

provisioner "file" {
  source = "generated/wget_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\wget_customize.ps1"
}
provisioner "file" {
  source = "generated/wget_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\wget_customize.vbs"
}

provisioner "file" {
  source = "generated/vscode_python_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\vscode_python_customize.ps1"
}
provisioner "file" {
  source = "generated/vscode_python_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\vscode_python_customize.vbs"
}

provisioner "file" {
  source = "generated/curl_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\curl_customize.ps1"
}
provisioner "file" {
  source = "generated/curl_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\curl_customize.vbs"
}

provisioner "file" {
  source = "generated/quick_access_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\quick_access_customize.ps1"
}
provisioner "file" {
  source = "generated/quick_access_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\quick_access_customize.vbs"
}

provisioner "file" {
  source = "generated/sysinternals_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\sysinternals_customize.ps1"
}
provisioner "file" {
  source = "generated/sysinternals_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\sysinternals_customize.vbs"
}

provisioner "file" {
  source = "generated/vscode_autoit_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\vscode_autoit_customize.ps1"
}
provisioner "file" {
  source = "generated/vscode_autoit_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\vscode_autoit_customize.vbs"
}

provisioner "file" {
  source = "generated/vscode_golang_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\vscode_golang_customize.ps1"
}
provisioner "file" {
  source = "generated/vscode_golang_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\vscode_golang_customize.vbs"
}

provisioner "file" {
  source = "generated/windows_explorer_customize_show_file_extensions.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\windows_explorer_customize_show_file_extensions.ps1"
}
provisioner "file" {
  source = "generated/windows_explorer_customize_show_file_extensions.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\windows_explorer_customize_show_file_extensions.vbs"
}

provisioner "file" {
  source = "generated/vscode_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\vscode_customize.ps1"
}
provisioner "file" {
  source = "generated/vscode_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\vscode_customize.vbs"
}

provisioner "file" {
  source = "generated/mysql_workbench_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\mysql_workbench_customize.ps1"
}
provisioner "file" {
  source = "generated/mysql_workbench_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\mysql_workbench_customize.vbs"
}

provisioner "file" {
  source = "generated/file_association_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\file_association_customize.ps1"
}
provisioner "file" {
  source = "generated/file_association_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\file_association_customize.vbs"
}

provisioner "file" {
  source = "generated/network_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\network_customize.ps1"
}
provisioner "file" {
  source = "generated/network_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\network_customize.vbs"
}

provisioner "file" {
  source = "generated/windows_indexing_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\windows_indexing_customize.ps1"
}
provisioner "file" {
  source = "generated/windows_indexing_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\windows_indexing_customize.vbs"
}

provisioner "file" {
  source = "generated/firewall_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\firewall_customize.ps1"
}
provisioner "file" {
  source = "generated/firewall_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\firewall_customize.vbs"
}

