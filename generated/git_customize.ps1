$base = (Get-Item $MyInvocation.MyCommand.Path).Basename
$dir = Split-Path -Parent -Path $MyInvocation.MyCommand.Path
$log_path = Join-Path $dir "logs_$($env:USERNAME)" | Join-Path -ChildPath "$($base).log"
New-Item -Force -Type "directory" (Split-Path -Parent -Path $log_path) | Out-Null
Start-Transcript -Path $log_path

# Set-PSdebug -Trace 1

git config --global user.email "taylormonacelli@gmail.com"
git config --global user.name "Taylor Monacelli"

$url = [System.Uri]'https://raw.githubusercontent.com/TaylorMonacelli/dotfiles/master/.gitconfig'
$gitconfig = Join-Path $env:USERPROFILE $url.Segments[-1]

Invoke-WebRequest -UseBasicParsing $url -OutFile $gitconfig
