$base = (Get-Item $MyInvocation.MyCommand.Path).Basename
$dir = Split-Path -Parent -Path $MyInvocation.MyCommand.Path
$log_path = Join-Path $dir "logs_$($env:USERNAME)" | Join-Path -ChildPath "$($base).log"
New-Item -Force -Type "directory" (Split-Path -Parent -Path $log_path) | Out-Null
Start-Transcript -Path $log_path

# Set-PSdebug -Trace 1

$psgallery = Get-PSRepository -ErrorAction SilentlyContinue -Name PSGallery

if(!$psgallery.Trusted) {
    Set-ExecutionPolicy -Force bypass
    Set-PSRepository -Name PSGallery -InstallationPolicy Trusted
}

Get-PSRepository -ErrorAction SilentlyContinue -Name PSGallery
