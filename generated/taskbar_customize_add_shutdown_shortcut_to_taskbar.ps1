$base = (Get-Item $MyInvocation.MyCommand.Path).Basename
$dir = Split-Path -Parent -Path $MyInvocation.MyCommand.Path
$log_path = Join-Path $dir "logs_$($env:USERNAME)" | Join-Path -ChildPath "$($base).log"
New-Item -Force -Type "directory" (Split-Path -Parent -Path $log_path) | Out-Null
Start-Transcript -Path $log_path

# Set-PSdebug -Trace 1

If (!$env:USERPROFILE) {
  exit 0
}

$resolved = Resolve-Path -ErrorAction SilentlyContinue ~
If ($resolved) {

  Import-Module "$env:ChocolateyInstall\helpers\chocolateyInstaller.psm1" -Force
  Install-ChocolateyShortcut `
    -Arguments '-NoProfile -NoLogo -Command "shutdown -t 0 -s"' `
    -IconLocation "C:\Windows\Temp\iconfinder_exit_17902.ico" `
    -ShortcutFilePath 'C:\Windows\Temp\shutdown_now.lnk' `
    -TargetPath "C:\WINDOWS\system32\WindowsPowerShell\v1.0\powershell.exe"

}
