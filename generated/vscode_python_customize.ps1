<# -*- mode: org -*- #>

<#

** start script

#>

$base = (Get-Item $MyInvocation.MyCommand.Path).Basename
$dir = Split-Path -Parent -Path $MyInvocation.MyCommand.Path
$log_path = Join-Path $dir "logs_$($env:USERNAME)" | Join-Path -ChildPath "$($base).log"
New-Item -Force -Type "directory" (Split-Path -Parent -Path $log_path) | Out-Null
Start-Transcript -Path $log_path

# Set-PSdebug -Trace 1

code --install-extension ms-python.python
code --install-extension ms-toolsai.jupyter

$script = Get-ChildItem -r $env:USERPROFILE\.vscode\extensions\ms-python.python-*\pythonFiles -Filter pyvsc-run-isolated.py
& python $script pip install -U black --user


$minimize = 'C:\Windows\Temp\Scripts\vscode_minimize.ps1'
&$minimize
code C:\Windows\Temp\Scripts\vscode_runtimeargs_tojson.py
Start-Sleep -Seconds 10
Stop-Process -Name code

<#

** end script

#>

<#

** notes

$script = Get-ChildItem -r $env:USERPROFILE\.vscode\extensions\ms-python.python-*\pythonFiles -Filter pyvsc-run-isolated.py
& python $script pip install -U black --user

& python c:\Users\taylor\.vscode\extensions\ms-python.python-2021.3.658691958\pythonFiles\pyvsc-run-isolated.py pip install -U black --user

PS C:\Users\taylor> & python c:\Users\taylor\.vscode\extensions\ms-python.python-2021.2.636928669\pythonFiles\pyvsc-run-isolated.py pip install -U pylint --user

Collecting pylint
  Downloading pylint-2.7.2-py3-none-any.whl (342 kB)
Collecting isort<6,>=4.2.5
  Downloading isort-5.7.0-py3-none-any.whl (104 kB)
Collecting astroid<2.6,>=2.5.1
  Downloading astroid-2.5.1-py3-none-any.whl (222 kB)
Collecting mccabe<0.7,>=0.6
  Downloading mccabe-0.6.1-py2.py3-none-any.whl (8.6 kB)
Collecting toml>=0.7.1
  Downloading toml-0.10.2-py2.py3-none-any.whl (16 kB)
Collecting colorama; sys_platform == "win32"
  Downloading colorama-0.4.4-py2.py3-none-any.whl (16 kB)
Collecting lazy-object-proxy>=1.4.0
  Downloading lazy_object_proxy-1.5.2-cp39-cp39-win_amd64.whl (20 kB)
Collecting wrapt<1.13,>=1.11
  Downloading wrapt-1.12.1.tar.gz (27 kB)
Using legacy 'setup.py install' for wrapt, since package 'wheel' is not installed.
Installing collected packages: isort, lazy-object-proxy, wrapt, astroid, mccabe, toml, colorama, pylint
  WARNING: The scripts isort-identify-imports.exe and isort.exe are installed in 'C:\Users\taylor\AppData\Roaming\Python\Python39\Scripts' which is not on PATH.
  Consider adding this directory to PATH or, if you prefer to suppress this warning, use --no-warn-script-location.
    Running setup.py install for wrapt ... done
  WARNING: The scripts epylint.exe, pylint.exe, pyreverse.exe and symilar.exe are installed in 'C:\Users\taylor\AppData\Roaming\Python\Python39\Scripts' which is not on PATH.
  Consider adding this directory to PATH or, if you prefer to suppress this warning, use --no-warn-script-location.
Successfully installed astroid-2.5.1 colorama-0.4.4 isort-5.7.0 lazy-object-proxy-1.5.2 mccabe-0.6.1 pylint-2.7.2 toml-0.10.2 wrapt-1.12.1
WARNING: You are using pip version 20.2.3; however, version 21.0.1 is available.
You should consider upgrading via the 'C:\Python39\python.exe -m pip install --upgrade pip' command.

#>
