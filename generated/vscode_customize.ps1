$base = (Get-Item $MyInvocation.MyCommand.Path).Basename
$dir = Split-Path -Parent -Path $MyInvocation.MyCommand.Path
$log_path = Join-Path $dir "logs_$($env:USERNAME)" | Join-Path -ChildPath "$($base).log"
New-Item -Force -Type "directory" (Split-Path -Parent -Path $log_path) | Out-Null
Start-Transcript -Path $log_path

# Set-PSdebug -Trace 1

# First time you open vscode you're greeted with a bunch of annoying
# messages and pop-ups attempt to get rid of those by openning vscode
# automatically with the assumption that the second time you open
# vscode, vscode won't show those popups again.

$minimize = 'C:\Windows\Temp\Scripts\vscode_minimize.exe'
&$minimize
code
Start-Sleep -Seconds 10
Get-Process | Where-Object { $_.Name -eq "Code" } | Stop-Process
