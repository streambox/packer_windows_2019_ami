<#

regjump HKCU:\Software\Sysinternals

#>

$block = {
    $list = @(
        "LogonSessions"
        "NotMyFault"
        "Process Explorer"
        "RegJump"
        "TCPView"
    )

    foreach ($key in $list){
        New-Item -Force -Path "HKCU:\Software\Sysinternals\$key"
        New-ItemProperty -Force -Path "HKCU:\Software\Sysinternals\$key" -Name EulaAccepted -Value 1
    }
}

$base = (Get-Item $MyInvocation.MyCommand.Path).Basename
$dir = Split-Path -Parent -Path $MyInvocation.MyCommand.Path
$log_path = Join-Path $dir "logs_$($env:USERNAME)" | Join-Path -ChildPath "$($base).log"
New-Item -Force -Type "directory" (Split-Path -Parent -Path $log_path) | Out-Null
Start-Transcript -Path $log_path

# Set-PSdebug -Trace 1

&$block
