$base = (Get-Item $MyInvocation.MyCommand.Path).Basename
$dir = Split-Path -Parent -Path $MyInvocation.MyCommand.Path
$log_path = Join-Path $dir "logs_$($env:USERNAME)" | Join-Path -ChildPath "$($base).log"
New-Item -Force -Type "directory" (Split-Path -Parent -Path $log_path) | Out-Null
Start-Transcript -Path $log_path

# Set-PSdebug -Trace 1

$A = New-ScheduledTaskAction -Execute 'C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe' `
 -WorkingDirectory "C:\Windows\Temp" `
 -Argument 'curl_customize.ps1'

$T = New-ScheduledTaskTrigger `
 -Once `
 -At (Get-Date) `
 -RepetitionInterval (New-TimeSpan -Minutes 5)

$P = New-ScheduledTaskPrincipal "Administrator"
$S = New-ScheduledTaskSettingsSet
$D = New-ScheduledTask -Action $A -Principal $P -Trigger $T -Settings $S

Register-ScheduledTask QuserWrapper -InputObject $D -Force






<#

Chicken scratch

Install-WindowsFeature RSAT
Add-WindowsCapability -Name Rsat.* -Online
Add-WindowsCapability -Name Rsat.CertificateServices.Tools~~~~0.0.1.0 -Online
Get-WindowsCapability -Name RSAT* -Online | Add-WindowsCapability –Online
Get-WindowsCapability -Name RSAT* -Online | Select-Object -Property DisplayName, State
Get-WindowsCapability -Name RSAT* -Online
Get-Command -Noun WindowsCapability
choco install rsat
chocolatey Remote Server Administration Toolss
install Remote Server Administration Tools

Find-Module GroupPolicy
Import-Module GroupPolicy

powershell import get-gpo
Import-GPO
New-GPO -Name TestGPO -Comment "This is a test GPO."
powershell create group policy objects
group policy login scripts
group policy login scripts
Group policy task first logon
task scheduler run task first login
task scheduler run task at user login
New-ScheduledTaskAction user login
New-ScheduledTaskAction

#>
