$base = (Get-Item $MyInvocation.MyCommand.Path).Basename
$dir = Split-Path -Parent -Path $MyInvocation.MyCommand.Path
$log_path = Join-Path $dir "logs_$($env:USERNAME)" | Join-Path -ChildPath "$($base).log"
New-Item -Force -Type "directory" (Split-Path -Parent -Path $log_path) | Out-Null
Start-Transcript -Path $log_path

# Set-PSdebug -Trace 1

choco install setdefaultbrowser

SetDefaultBrowser HKLM "Google Chrome"

# exhaust showing spash screen
Start-Process -WindowStyle Minimized -FilePath 'C:\Program Files\Google\Chrome\Application\chrome.exe' -ArgumentList 'https://google.com'

If($env:USERPROFILE){
    Remove-Item -Force -ErrorAction SilentlyContinue "$env:USERPROFILE\Desktop\Google Chrome.lnk" | Out-Null
}
