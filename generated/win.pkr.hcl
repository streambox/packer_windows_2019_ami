variable "aws_region" {
  type    = string
  default = "us-east-2"
}

variable "instance_type" {
  type    = string
  default = "t3a.medium"
}

variable "subnet_id" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "ami_users" {
  type = string
}

source "amazon-ebs" "windows_server" {
  ami_description             = "A custom Windows Server AMI"
  ami_name                    = "windows-example"
  ami_users                   = ["${var.ami_users}"]
  associate_public_ip_address = true
  communicator                = "winrm"
  instance_type               = "${var.instance_type}"
  region                      = "${var.aws_region}"
  force_deregister            = true
  force_delete_snapshot       = true
  source_ami_filter {
    filters = {
      architecture        = "x86_64"
      name                = "Windows_Server-2019-English-Full-ContainersLatest-*"
      root-device-type    = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners      = ["801119661308"]
  }
  subnet_id      = "${var.subnet_id}"
  user_data_file = "./bootstrap_win.txt"
  vpc_id         = "${var.vpc_id}"
  winrm_insecure = true
  winrm_port     = 5986
  winrm_use_ssl  = true
  winrm_username = "Administrator"
}

build {
  sources = ["source.amazon-ebs.windows_server"]
  
  provisioner "file"{
    source = "powershell_update_help.ps1"
    destination = "C:\\Windows\\Temp\\scripts\\powershell_update_help.ps1"
  }

provisioner "file" {
  source = "vscode_customize_all.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\vscode_customize_all.ps1"
}
provisioner "file" {
  source = "vscode_customize_all.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\vscode_customize_all.vbs"
}


provisioner "file" {
  source = "generated/psgallery_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\psgallery_customize.ps1"
}
provisioner "file" {
  source = "generated/psgallery_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\psgallery_customize.vbs"
}

provisioner "file" {
  source = "generated/windows_explorer_customize_stop_windows_explorer.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\windows_explorer_customize_stop_windows_explorer.ps1"
}
provisioner "file" {
  source = "generated/windows_explorer_customize_stop_windows_explorer.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\windows_explorer_customize_stop_windows_explorer.vbs"
}

provisioner "file" {
  source = "generated/vscode_default_settings_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\vscode_default_settings_customize.ps1"
}
provisioner "file" {
  source = "generated/vscode_default_settings_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\vscode_default_settings_customize.vbs"
}

provisioner "file" {
  source = "generated/powershell_core_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\powershell_core_customize.ps1"
}
provisioner "file" {
  source = "generated/powershell_core_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\powershell_core_customize.vbs"
}

provisioner "file" {
  source = "generated/golang_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\golang_customize.ps1"
}
provisioner "file" {
  source = "generated/golang_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\golang_customize.vbs"
}

provisioner "file" {
  source = "generated/vscode_powershell_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\vscode_powershell_customize.ps1"
}
provisioner "file" {
  source = "generated/vscode_powershell_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\vscode_powershell_customize.vbs"
}

provisioner "file" {
  source = "generated/git_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\git_customize.ps1"
}
provisioner "file" {
  source = "generated/git_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\git_customize.vbs"
}

provisioner "file" {
  source = "generated/powershell_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\powershell_customize.ps1"
}
provisioner "file" {
  source = "generated/powershell_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\powershell_customize.vbs"
}

provisioner "file" {
  source = "generated/powershell_customize_windows.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\powershell_customize_windows.ps1"
}
provisioner "file" {
  source = "generated/powershell_customize_windows.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\powershell_customize_windows.vbs"
}

provisioner "file" {
  source = "generated/customize_during_first_login.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\customize_during_first_login.ps1"
}
provisioner "file" {
  source = "generated/customize_during_first_login.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\customize_during_first_login.vbs"
}

provisioner "file" {
  source = "generated/powershell_customize_profile.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\powershell_customize_profile.ps1"
}
provisioner "file" {
  source = "generated/powershell_customize_profile.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\powershell_customize_profile.vbs"
}

provisioner "file" {
  source = "generated/vscode_customize_add_vim_extension.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\vscode_customize_add_vim_extension.ps1"
}
provisioner "file" {
  source = "generated/vscode_customize_add_vim_extension.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\vscode_customize_add_vim_extension.vbs"
}

provisioner "file" {
  source = "generated/google_chrome_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\google_chrome_customize.ps1"
}
provisioner "file" {
  source = "generated/google_chrome_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\google_chrome_customize.vbs"
}

provisioner "file" {
  source = "generated/desktop_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\desktop_customize.ps1"
}
provisioner "file" {
  source = "generated/desktop_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\desktop_customize.vbs"
}

provisioner "file" {
  source = "generated/windows_explorer_customize_show_detailed_view.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\windows_explorer_customize_show_detailed_view.ps1"
}
provisioner "file" {
  source = "generated/windows_explorer_customize_show_detailed_view.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\windows_explorer_customize_show_detailed_view.vbs"
}

provisioner "file" {
  source = "generated/taskbar_customize_add_shutdown_shortcut_to_taskbar.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\taskbar_customize_add_shutdown_shortcut_to_taskbar.ps1"
}
provisioner "file" {
  source = "generated/taskbar_customize_add_shutdown_shortcut_to_taskbar.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\taskbar_customize_add_shutdown_shortcut_to_taskbar.vbs"
}

provisioner "file" {
  source = "generated/powershell_customize_macos.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\powershell_customize_macos.ps1"
}
provisioner "file" {
  source = "generated/powershell_customize_macos.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\powershell_customize_macos.vbs"
}

provisioner "file" {
  source = "generated/taskbar_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\taskbar_customize.ps1"
}
provisioner "file" {
  source = "generated/taskbar_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\taskbar_customize.vbs"
}

provisioner "file" {
  source = "generated/wget_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\wget_customize.ps1"
}
provisioner "file" {
  source = "generated/wget_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\wget_customize.vbs"
}

provisioner "file" {
  source = "generated/vscode_python_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\vscode_python_customize.ps1"
}
provisioner "file" {
  source = "generated/vscode_python_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\vscode_python_customize.vbs"
}

provisioner "file" {
  source = "generated/curl_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\curl_customize.ps1"
}
provisioner "file" {
  source = "generated/curl_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\curl_customize.vbs"
}

provisioner "file" {
  source = "generated/quick_access_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\quick_access_customize.ps1"
}
provisioner "file" {
  source = "generated/quick_access_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\quick_access_customize.vbs"
}

provisioner "file" {
  source = "generated/sysinternals_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\sysinternals_customize.ps1"
}
provisioner "file" {
  source = "generated/sysinternals_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\sysinternals_customize.vbs"
}

provisioner "file" {
  source = "generated/vscode_autoit_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\vscode_autoit_customize.ps1"
}
provisioner "file" {
  source = "generated/vscode_autoit_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\vscode_autoit_customize.vbs"
}

provisioner "file" {
  source = "generated/vscode_golang_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\vscode_golang_customize.ps1"
}
provisioner "file" {
  source = "generated/vscode_golang_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\vscode_golang_customize.vbs"
}

provisioner "file" {
  source = "generated/windows_explorer_customize_show_file_extensions.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\windows_explorer_customize_show_file_extensions.ps1"
}
provisioner "file" {
  source = "generated/windows_explorer_customize_show_file_extensions.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\windows_explorer_customize_show_file_extensions.vbs"
}

provisioner "file" {
  source = "generated/vscode_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\vscode_customize.ps1"
}
provisioner "file" {
  source = "generated/vscode_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\vscode_customize.vbs"
}

provisioner "file" {
  source = "generated/mysql_workbench_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\mysql_workbench_customize.ps1"
}
provisioner "file" {
  source = "generated/mysql_workbench_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\mysql_workbench_customize.vbs"
}

provisioner "file" {
  source = "generated/file_association_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\file_association_customize.ps1"
}
provisioner "file" {
  source = "generated/file_association_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\file_association_customize.vbs"
}

provisioner "file" {
  source = "generated/network_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\network_customize.ps1"
}
provisioner "file" {
  source = "generated/network_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\network_customize.vbs"
}

provisioner "file" {
  source = "generated/windows_indexing_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\windows_indexing_customize.ps1"
}
provisioner "file" {
  source = "generated/windows_indexing_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\windows_indexing_customize.vbs"
}

provisioner "file" {
  source = "generated/firewall_customize.ps1"
  destination = "C:\\Windows\\Temp\\scripts\\firewall_customize.ps1"
}
provisioner "file" {
  source = "generated/firewall_customize.vbs"
  destination = "C:\\Windows\\Temp\\scripts\\firewall_customize.vbs"
}



  provisioner "file" {
    source = "vscode_minimize.ps1"
    destination = "C:\\Windows\\Temp\\scripts\\vscode_minimize.ps1"
  }
  provisioner "file" {
    source = "Microsoft.PowerShell_profile.ps1"
    destination = "C:\\Windows\\Temp\\scripts\\Microsoft.PowerShell_profile.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./dotnet_securityprotocol.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./chocolatey.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./sysinternals.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./google_chrome.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./generated/google_chrome_customize.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./awscli.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./visual_studio.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./signtool.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./skype.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./curl.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./wget.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./git.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./nuget.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./rsat.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./psgallery.ps1"
  }
  provisioner "shell-local" {
    script = "./creds_setup.sh"
  }
  provisioner "file" {
    destination = "C:\\ProgramData\\secrets.json"
    source      = "./secrets.json"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./wixtoolset.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./openssh.ps1"
  }

  # provisioner "powershell" {
  #   environment_vars = ["WINRMPASS=${build.Password}"]
  #   inline = ["Write-Host \"Automatically generated aws password is: $Env:WINRMPASS\""]
  # }

  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./pester.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./powershell_update_help.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./streambox_mediaplayer.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./ping.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./powershell_core.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./openssh_with_powershell_core.ps1"
#    script            = "./openssh_with_powershell.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./generated/powershell_customize.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./generated/windows_indexing_customize.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./python.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./mysql_workbench.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./windows_defender_disable.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./disable_uac.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./autoit.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./vscode_minimize.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./vscode.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./firefox.ps1"
  }
  # provisioner "powershell" {
  #   elevated_user     = "SYSTEM"
  #   elevated_password = ""
  #   script            = "./perl.ps1"
  # }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./bazel.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./7zip.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./wireshark.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./cygwin.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./ec2config.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./neovim.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./windows_terminal.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./golang.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./add_user.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./filelist.ps1"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./generated/runonce_customize.ps1"
  }
  provisioner "file" {
    source = "vscode_runtimeargs_tojson.py"
    destination = "C:\\Windows\\Temp\\scripts\\vscode_runtimeargs_tojson.py"
  }
  provisioner "file" {
    source = "resources/iconfinder_exit_17902.ico"
    destination = "C:\\Windows\\Temp\\iconfinder_exit_17902.ico"
  }
  provisioner "powershell" {
    elevated_user     = "SYSTEM"
    elevated_password = ""
    script            = "./generated/taskbar_customize.ps1"
  }
  provisioner "powershell" {
    inline = [
      "C:\\ProgramData\\Amazon\\EC2-Windows\\Launch\\Scripts\\SendWindowsIsReady.ps1 -Schedule"
    ]
  }
  provisioner "powershell" {
    inline = [
      "C:\\ProgramData\\Amazon\\EC2-Windows\\Launch\\Scripts\\InitializeInstance.ps1 -Schedule"
    ]
  }

  # provisioner "powershell" {
  #   elevated_user     = "SYSTEM"
  #   elevated_password = ""
  #   script            = "./sysprep.ps1"
  # }
}
