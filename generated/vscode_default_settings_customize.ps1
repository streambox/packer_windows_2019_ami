<#

If(Test-Path("$env:USERPROFILE\.vscode")){ Remove-Item -Force -Recurse $env:USERPROFILE\.vscode }
start "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Visual Studio Code\Visual Studio Code.lnk"

New-Item -Force -Type "directory" $env:APPDATA\Code\User | Out-Null

# everything
Get-Process | Where-Object { $_.Name -eq "code" } | Stop-Process
Start-Sleep -Seconds 10
Remove-Item -Force -Recurse $env:USERPROFILE\.vscode
Remove-Item -Force -Recurse $env:APPDATA\Code
Start "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Visual Studio Code\Visual Studio Code.lnk"
Start-Sleep -Seconds 10
Get-Process | Where-Object { $_.Name -eq "code" } | Stop-Process
code

# single settings files
Remove-Item -Force $env:APPDATA\Code\User\settings.json
Remove-Item -Force $env:USERPROFILE\.vscode\argv.json

python vscode_runtimeargs_tojson.py $env:USERPROFILE\.vscode\argv.json
python c:\Windows\Temp\Scripts\vscode_runtimeargs_tojson.py $env:USERPROFILE\.vscode\argv.json

start $env:APPDATA\Code\User
Remove-Item -Force -Recurse $env:USERPROFILE\.vscode
start $env:USERPROFILE\.vscode
jq edit file in place
yq edit file in place
choco install yq
yq w $env:USERPROFILE\.vscode\argv.json telemetry.enableTelemetry true

yq w pod.yaml "spec.containers[0].env[0].value" "postgres://prod:5432"
apiVersion: v1
kind: Pod
meta

#>

$base = (Get-Item $MyInvocation.MyCommand.Path).Basename
$dir = Split-Path -Parent -Path $MyInvocation.MyCommand.Path
$log_path = Join-Path $dir "logs_$($env:USERNAME)" | Join-Path -ChildPath "$($base).log"
New-Item -Force -Type "directory" (Split-Path -Parent -Path $log_path) | Out-Null
Start-Transcript -Path $log_path

# Set-PSdebug -Trace 1

$minimize = 'C:\Windows\Temp\Scripts\vscode_minimize.ps1'
&$minimize
code

Start-Sleep -Seconds 10

python C:\Windows\Temp\Scripts\vscode_runtimeargs_tojson.py $env:USERPROFILE\.vscode\argv.json
