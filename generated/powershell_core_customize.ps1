$base = (Get-Item $MyInvocation.MyCommand.Path).Basename
$dir = Split-Path -Parent -Path $MyInvocation.MyCommand.Path
$log_path = Join-Path $dir "logs_$($env:USERNAME)" | Join-Path -ChildPath "$($base).log"
New-Item -Force -Type "directory" (Split-Path -Parent -Path $log_path) | Out-Null
Start-Transcript -Path $log_path

# Set-PSdebug -Trace 1

If (!$env:USERPROFILE) {
  exit 0
}

$core_profile = Join-Path $env:USERPROFILE "Documents\PowerShell\Microsoft.PowerShell_profile.ps1"

# C:\Users\taylor\Documents\PowerShell\Microsoft.PowerShell_profile.ps1

New-Item -Force -Type "directory" (Split-Path -Parent $core_profile) | Out-Null
Get-ChildItem C:\Windows\Temp\scripts\Microsoft.PowerShell_profile.ps1
Copy-Item "C:\Windows\Temp\scripts\Microsoft.PowerShell_profile.ps1" $core_profile

# FIXME: we can't use ~ when using SYSTEM in packer
# https://ci.appveyor.com/project/TaylorMonacelli/packer-windows-2019-ami/builds/38014468#L620


$resolved = Resolve-Path -ErrorAction SilentlyContinue ~
If ($resolved) {

  Import-Module "$env:ChocolateyInstall\helpers\chocolateyInstaller.psm1" -Force
  Install-ChocolateyShortcut `
    -Arguments '-NoLogo -NoExit -Command Set-Location ~' `
    -RunAsAdmin `
    -ShortcutFilePath "C:\Program Files\PowerShell\7\pwsh.lnk" `
    -TargetPath "C:\Program Files\PowerShell\7\pwsh.exe"


}

# first time you run pwsh, it seems to delay a while doing stuff, so
#  lets start it automatically so we can exhaust the delay
Start-Process -WindowStyle Minimized "C:\Program Files\PowerShell\7\pwsh.lnk"
