$base = (Get-Item $MyInvocation.MyCommand.Path).Basename
$dir = Split-Path -Parent -Path $MyInvocation.MyCommand.Path
$log_path = Join-Path $dir "logs_$($env:USERNAME)" | Join-Path -ChildPath "$($base).log"
New-Item -Force -Type "directory" (Split-Path -Parent -Path $log_path) | Out-Null
Start-Transcript -Path $log_path

# Set-PSdebug -Trace 1

Import-Module "$env:ChocolateyInstall\helpers\chocolateyInstaller.psm1" -Force
Install-ChocolateyShortcut `
    -Arguments '-NoLogo -NoExit -Command Set-Location ~' `
    -RunAsAdmin `
    -ShortcutFilePath "C:\Windows\System32\windowspowershell\v1.0\powershell.lnk" `
    -TargetPath "C:\Windows\System32\windowspowershell\v1.0\powershell.exe"




<#
start C:\Windows\System32\windowspowershell\v1.0
start c:\Windows\temp

#>

exit






<#

FAIL: -PinToTaskbar
PASS: -RunAsAdmin


Install-ChocolateyShortcut `
    -Arguments '-NoLogo -NoExit -Command Set-Location ~' `
    -PinToTaskbar `
    -RunAsAdmin `
    -ShortcutFilePath "C:\Windows\System32\windowspowershell\v1.0\powershell.lnk" `
    -TargetPath "C:\Windows\System32\windowspowershell\v1.0\powershell.exe"

#>









# https://www.norlunn.net/2019/10/07/powershell-customize-the-prompt/

<#

start C:\Windows\System32\windowspowershell\v1.0

Create shortcut:
C:\Windows\System32\windowspowershell\v1.0\powershell.lnk
cd C:\Windows\System32\windowspowershell\v1.0
start .
ls C:\Windows\System32\windowspowershell\v1.0\powershell.lnk

and set property "Run As Administrator" on

#>

$installDir = "C:\Windows\System32\windowspowershell\v1.0"
$ShCutLnk = "powershell.lnk"

$WshShell = New-Object -comObject WScript.Shell
$Shortcut = $WshShell.CreateShortcut("$installDir\$ShCutLnk")
$Shortcut.TargetPath = "C:\Windows\System32\windowspowershell\v1.0\powershell.exe"
$Shortcut.IconLocation = "C:\Windows\System32\windowspowershell\v1.0\powershell.exe,0" # icon index 0
$Shortcut.Arguments = "-NoLogo -NoExit -Command Set-Location ~"
$Shortcut.WorkingDirectory = "$installDir"
$Shortcut.Save()

## Make the Shortcut runas Administrator
## Source: https://stackoverflow.com/questions/28997799/how-to-create-a-run-as-administrator-shortcut-using-powershell
$bytes = [System.IO.File]::ReadAllBytes("$installDir\$ShCutLnk")
$bytes[0x15] = $bytes[0x15] -bor 0x20 #set byte 21 (0x15) bit 6 (0x20) ON
[System.IO.File]::WriteAllBytes("$installDir\$ShCutLnk", $bytes)
