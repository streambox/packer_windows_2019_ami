<#

https://marketplace.visualstudio.com/items?itemName=golang.Go#tools

The extension uses a few command-line tools developed by the Go
community.

In particular:

go
gopls
dlv 

gopkgs
go-outline
golint

must be installed for this extension to work correctly.

See the tools documentation for a complete list of tools the extension
depends on.

In order to locate these command-line tools, the extension searches
GOPATH/bin and directories specified in the PATH environment variable
(or Path on Windows) with which the VS Code process has started.

If the tools are not found, the extension will prompt you to install
the missing tools and show the "⚠️ Analysis Tools Missing" warning in
the bottom right corner.

Please install them by responding to the warning notification, or by
manually running the Go: Install/Update Tools command.

# googling
install golang dlv
install gopls
vscode install go analysis tools
install gopkgs go-outline
install gopkgs go-outline offline
install golang dlv
golint


https://github.com/golang/vscode-go/blob/master/docs/tools.md
https://github.com/golang/vscode-go/blob/master/README.md#quick-start
https://github.com/golang/vscode-go/blob/master/README.md#tools
https://github.com/go-delve/delve/blob/master/Documentation/installation/README.md
https://github.com/golang/vscode-go/blob/master/docs/tools.md
https://medium.com/the-andela-way/gopls-language-server-setup-for-go-projects-3ee79dcac123
https://github.com/golang/lint

go get github.com/tpng/gopkgs

install gopkgs
install golint
install go-outline

go get -u github.com/ramya-rao-a/go-outline
go install github.com/ramya-rao-a/go-outline@latest

chocolatey golint

install golint
go install github.com/go-delve/delve/cmd/golint@latest

https://github.com/golang/tools/blob/master/gopls/README.md#installation
GO111MODULE=on go get golang.org/x/tools/gopls@latest
$env:GO111MODULE=on
go get golang.org/x/tools/gopls@latest

go install github.com/ramya-rao-a/go-outline@latest

workspace: err: exit status 1: stderr: go: gopkg.in/check.v1@v0.0.0-20161208181325-20d25e280405: missing go.sum entry; 
to add it: go mod download gopkg.in/check.v1 : packages.Load error

# Invoke-Command -ComputerName localhost -ScriptBlock $block -JobName golang -AsJob
If(Test-Path('./go')){ Remove-Item -Force -Recurse ./go }
Start-Job -Name golangsetup -Scriptblock $block
Start-Sleep -Seconds 5
Get-Job

Windows minimize window commandline

#>

@'
code --list-extensions
code --install-extension golang.go

# vscode complains with these tools, but i don't know if this is
# acceptable way to fetch them from cli, iow, am I pulling bits from
# proper place? dunno
go install github.com/go-delve/delve/cmd/dlv@latest
go install github.com/ramya-rao-a/go-outline@latest
go install github.com/tpng/gopkgs@latest
go install golang.org/x/lint/golint@latest
go install golang.org/x/tools/gopls@latest
'@ | Out-File -encoding ASCII C:\Windows\temp\scripts\golang_fetch.ps1

$base = (Get-Item $MyInvocation.MyCommand.Path).Basename
$dir = Split-Path -Parent -Path $MyInvocation.MyCommand.Path
$log_path = Join-Path $dir "logs_$($env:USERNAME)" | Join-Path -ChildPath "$($base).log"
New-Item -Force -Type "directory" (Split-Path -Parent -Path $log_path) | Out-Null
Start-Transcript -Path $log_path

# Set-PSdebug -Trace 1

go get -v golang.org/x/tools/gopls
go get -v honnef.co/go/tools/cmd/staticcheck

$minimize = 'C:\Windows\Temp\Scripts\vscode_minimize.exe'
&$minimize
&C:\Windows\temp\scripts\golang_fetch.ps1

$minimize = 'C:\Windows\Temp\Scripts\vscode_minimize.ps1'
$gofile = ls $env:USERPROFILE\go\pkg\mod\golang.org\x\*\*\*.go | Select-Object -Exp Fullname | Select-Object -First 1

<# 

todo
https://github.com/golang/vscode-go/blob/master/README.md

#>

&$minimize
code $gofile
