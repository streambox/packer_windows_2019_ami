<#

# Googling
New-ItemProperty hideFileExt

https://www.kittell.net/code/powershell-show-file-extensions/
regjump HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced
Set-ItemProperty HideFileExt
REG_DWORD

Get-ItemProperty

PS C:\> (Get-ItemProperty -Path "HKLM:\System\CurrentControlSet\Control\RadioManagement\SystemRadioState").'(default)'

#>

$base = (Get-Item $MyInvocation.MyCommand.Path).Basename
$dir = Split-Path -Parent -Path $MyInvocation.MyCommand.Path
$log_path = Join-Path $dir "logs_$($env:USERNAME)" | Join-Path -ChildPath "$($base).log"
New-Item -Force -Type "directory" (Split-Path -Parent -Path $log_path) | Out-Null
Start-Transcript -Path $log_path

# Set-PSdebug -Trace 1

# Set-ItemProperty -Path HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced -Name HideFileExt -Type DWord -Value 1
Set-ItemProperty -Path HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced -Name HideFileExt -Type DWord -Value 0
Get-ItemProperty -Path HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced -Name HideFileExt
# requires explorer restart. stopping it will trigger it to restart on its own: Stop-Process -Name explorer -Force
