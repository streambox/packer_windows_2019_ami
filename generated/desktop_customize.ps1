$base = (Get-Item $MyInvocation.MyCommand.Path).Basename
$dir = Split-Path -Parent -Path $MyInvocation.MyCommand.Path
$log_path = Join-Path $dir "logs_$($env:USERNAME)" | Join-Path -ChildPath "$($base).log"
New-Item -Force -Type "directory" (Split-Path -Parent -Path $log_path) | Out-Null
Start-Transcript -Path $log_path

# Set-PSdebug -Trace 1

# Wait for chrome to start up so that I can delete the shortcut
Start-Sleep -Seconds 20

$links = @();

$links += Get-ChildItem -Filter "*.website" -Recurse ~\Desktop
$links += Get-ChildItem -Filter "*.lnk" -Recurse C:\Users\Public\Desktop

foreach ($link in $links){
    Remove-Item -Force $link.Fullname
}
