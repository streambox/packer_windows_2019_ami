$base = (Get-Item $MyInvocation.MyCommand.Path).Basename
$dir = Split-Path -Parent -Path $MyInvocation.MyCommand.Path
$log_path = Join-Path $dir "logs_$($env:USERNAME)" | Join-Path -ChildPath "$($base).log"
New-Item -Force -Type "directory" (Split-Path -Parent -Path $log_path) | Out-Null
Start-Transcript -Path $log_path

# Set-PSdebug -Trace 1

$o = New-Object -com shell.application
$o.Namespace('C:\Program Files').Self.InvokeVerb("pintohome")
$o.Namespace('C:\Program Files (x86)').Self.InvokeVerb("pintohome")
$o.Namespace('C:\Windows\Temp\scripts').Self.InvokeVerb("pintohome")
$o.Namespace($env:USERPROFILE).Self.InvokeVerb("pintohome")
$o.Namespace('C:\ProgramData').Self.InvokeVerb("pintohome") # fails

<#

pin programdata folder quick access

powershell unhide programdata

# this unhides ProgramData folder
Set-ItemProperty -Path 'C:\ProgramData' -Name Attributes -Value Normal

but unhiding is not enough in order to enable me to pin programdata
folder to quick access...apparently

#>
