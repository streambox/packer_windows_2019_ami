$base = (Get-Item $MyInvocation.MyCommand.Path).Basename
$dir = Split-Path -Parent -Path $MyInvocation.MyCommand.Path
$log_path = Join-Path $dir "logs_$($env:USERNAME)" | Join-Path -ChildPath "$($base).log"
New-Item -Force -Type "directory" (Split-Path -Parent -Path $log_path) | Out-Null
Start-Transcript -Path $log_path

# Set-PSdebug -Trace 1


@'
Stop-Process -Name explorer -Force
# Remove-Item -Force $env:USERPROFILE\Desktop\Stop-Explorer.lnk
'@ | Out-File -Encoding ASCII C:\Windows\Temp\Scripts\Stop-Explorer.ps1

# https://docs.chocolatey.org/en-us/create/functions/install-chocolateyshortcut#windowstyle-int32
# minimized = 7
$window_style = 7

Import-Module "$env:ChocolateyInstall\helpers\chocolateyInstaller.psm1" -Force
Install-ChocolateyShortcut `
    -WindowStyle $window_style `
    -Arguments "& 'C:\Windows\Temp\Scripts\Stop-Explorer.ps1'" `
    -ShortcutFilePath "$env:USERPROFILE\Desktop\Stop-Explorer.lnk" `
    -TargetPath "C:\Windows\System32\windowspowershell\v1.0\powershell.exe"
