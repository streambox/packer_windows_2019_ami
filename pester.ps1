$mod = Get-InstalledModule -ErrorAction SilentlyContinue Pester

If(!$mod) {
    Install-Module Pester -Force -SkipPublisherCheck
}
