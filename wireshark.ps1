choco install winpcap
choco install wireshark

$dir = (Get-ChildItem -Recurse C:\Program*\Wireshark -Filter "tshark.exe" | select-object -first 1).Directory.FullName

$CurrentValue = [Environment]::GetEnvironmentVariable("PATH", "Machine")
$prefix = @{$true='';$false=';'}[$CurrentValue -eq '']
[Environment]::SetEnvironmentVariable("PATH", $CurrentValue + "$($prefix)$($dir)", "Machine")
