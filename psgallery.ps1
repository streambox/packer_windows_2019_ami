$psgallery = Get-PSRepository -ErrorAction SilentlyContinue -Name PSGallery

if(!$psgallery.Trusted) {
    Set-ExecutionPolicy -Force bypass
    Set-PSRepository -Name PSGallery -InstallationPolicy Trusted
}
