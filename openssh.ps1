Set-ExecutionPolicy -Force bypass

Set-Service -Name wuauserv -StartupType Automatic
Set-Service -Name BITS -StartupType Automatic
Restart-Service -Name wuauserv
Restart-Service -Name BITS

# https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_install_firstuse
Get-WindowsCapability -Online | ? Name -like 'OpenSSH*'
Add-WindowsCapability -Online -Name OpenSSH.Client~~~~0.0.1.0
Add-WindowsCapability -Online -Name OpenSSH.Server~~~~0.0.1.0

# Install the OpenSSHUtils module to the server. This will be valuable when deploying user keys.

# deprecated
# https://www.google.com/search?q=where+OpenSSHUtils&oq=where+OpenSSHUtils&aqs=chrome..69i57.1408j0j7&sourceid=chrome&ie=UTF-8
# deprecated Install-Module -Force OpenSSHUtils -Scope AllUsers

# By default the ssh-agent service is disabled. Allow it to be manually started for the next step to work.
Get-Service -Name ssh-agent | Set-Service -StartupType Manual

# Start the ssh-agent service to preserve the server keys
Start-Service ssh-agent

# Now start the sshd service
Start-Service sshd

Set-Service -Name sshd -StartupType 'Automatic'

$result = Get-NetFirewallRule -Name *ssh*
$result = New-NetFirewallRule -Name sshd -DisplayName 'OpenSSH Server (sshd)' -Enabled True -Direction Inbound -Protocol TCP -Action Allow -LocalPort 22

ls C:\ProgramData\ssh
cd C:\ProgramData\ssh

New-Item -Type "directory" -Force C:\ProgramData\ssh | Out-Null
@'
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFp48yQviDu3U2mGdwv7CO3O84IAj4LJUXzyGbs6mT0q mtm
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDhNtlfnv/yHQby42b6egPxL5SP3zYlB6ZqnsBrLadK/dfae+bQyn/PE/sRPUEIhaHKNxnaM4R7m6kfxLGwXB1k9eGYLpjDYFSzhKIpDwBHdsTsMNSgCYFwUvVQSoQtah7hbljK02kbfEaQMJ5VkbnyHZf2W0yj8lHf9vWJaeEJ+aXq+vIkEEV6aa+wsQyLaKWIPfPPzxDR4xRb5sCDc6vkHTZVepLelWRGk85l35+Wxwdv1rFhY2EYd5AeDbH7h0fCsjEbg2lfWICSs8tb/lCcWsaEXwvS4BX3jwzWAT+EG/2qBzSSeXiNS43tkSWhxeI9IJbz7BnW+LAdvF9/WGMBU8KPS6TDoU7l6//EKzVkLJlFcuD7Nb7Gguog7QlVlT7tNWH8zagOktvwU+czpoaKAm4gLAmNLMEaxQU+StTahQSOELaxTTV2Ac7ferMR2IVEoLRkaRe2faJ5iSbkRNR6yVXEUYDfDTvLgiSJzaVnzMb1XrHV42F6s02MKOXTTjJaCM3FuKPHY4e6fHn+xIh/VlKISjE6xJV7cpOdSGiVOwI7SeZiOjw4JnS3+BsGiUjqpl4nMvV1o4atZtD9Q8N4D/UQiRHt11EGUt4qrJGuuTsYeKHMaFEyeQ1lkRe3Riw/zd4OD00KSd4bSmKoziCUCaPEAYoZiVekd9YY1+BwjQ== anp
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMx0K9m51z/LRhu98QZnFdGpnwALkHNlOcsKHxsmSGe2 anp
'@ | Out-File -Encoding ASCII C:\ProgramData\ssh\administrators_authorized_keys
cat C:\ProgramData\ssh\administrators_authorized_keys

ls C:\ProgramData\ssh\administrators_authorized_keys

$acl = Get-Acl C:\ProgramData\ssh\administrators_authorized_keys
$acl.SetAccessRuleProtection($true, $false)
$administratorsRule = New-Object system.security.accesscontrol.filesystemaccessrule("Administrators","FullControl","Allow")
$systemRule = New-Object system.security.accesscontrol.filesystemaccessrule("SYSTEM","FullControl","Allow")
$acl.SetAccessRule($administratorsRule)
$acl.SetAccessRule($systemRule)
$acl | Set-Acl

Restart-Service -Name sshd
