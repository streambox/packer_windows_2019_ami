# https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_server_configuration

# New-ItemProperty -Path "HKLM:\SOFTWARE\OpenSSH" -Name DefaultShell -Value "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" -PropertyType String -Force

<# workaround: https://github.com/PowerShell/Win32-OpenSSH/issues/1677
#>
@'
@echo off

C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe -NoLogo
rem C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe
rem powershell -NoLogo
rem C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe -NoLogo
'@ | Out-File -Encoding ASCII 'C:\Windows\System32\WindowsPowerShell\v1.0\powershell.bat'
New-ItemProperty -Path "HKLM:\SOFTWARE\OpenSSH" -Name DefaultShell -Value "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.bat" -PropertyType String -Force
# New-ItemProperty -Path "HKLM:\SOFTWARE\OpenSSH" -Name DefaultShell -Value "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" -PropertyType String -Force
# cat C:\Windows\System32\WindowsPowerShell\v1.0\powershell.bat

(Get-ItemProperty HKLM:\SOFTWARE\OpenSSH).DefaultShellCommandOption



<#

ansible internal error: no input given to PowerShell exec wrapper

ansible twuseast2 -m win_shell -a 'ls'

twuseast2 | FAILED | rc=-1 >>
internal error: no input given to PowerShell exec wrapper

(.venv)(.py39) [mtm@taylorm:ansible(master)]$

#>
