$url = "https://streambox-taylor.s3-us-west-2.amazonaws.com/cygwinsetup_v1.0.1.0.exe"
$url = [System.Uri]$url
$wc = New-Object System.Net.WebClient
$installer = Split-Path -Leaf -Path $url.AbsolutePath
$installer_path = "$env:TEMP\$installer"
if(Test-Path($installer_path)){remove-item -force $installer_path}
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
$wc.DownloadFile($url, $installer_path)
Start-Process -Wait -FilePath $installer_path -ArgumentList /S
