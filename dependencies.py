import matplotlib.pyplot
import networkx
import networkx.drawing.nx_agraph

"""
python dependencies.py
cat test.dot | dot -Tpng >out.png && open out.png

networkx.DiGraph tree
networkx.DiGraph no edges
networkx.DiGraph visit each node
networkx cluster
G.add_node("")
G.add_edge("","")
"""

G = networkx.DiGraph()

G.add_node("ROOT")

G.add_edge("appveyor build", "runme.py")

G.add_edge("windows_defender_disable.ps1", "disable_uac.ps1", style='dashed')
G.add_edge("windows_defender_disable.ps1", "chocolatey.ps1")

G.add_edge("vscode:\ninstall extensions", "vscode.ps1")
G.add_edge("windows_explorer_customize_stop_windows_explorer.ps1", "chocolatey.ps1")

G.add_edge("vscode_python_customize.ps1", "vscode_minimize.ps1")

G.add_edge("vscode_customize_add_vim_extension.ps1", "vscode.ps1")

G.add_edge("taskbar_customize.ps1.tmpl", "taskbar_customize_add_shutdown_shortcut_to_taskbar.ps1.tmpl")

G.add_edge("refresh windows explorer", "windows_explorer_customize_stop_windows_explorer.ps1")

G.add_edge("run script at user logon", "windows_explorer_customize.ps1")
G.add_edge("run script at user logon", "powershell_update_help.ps1")
G.add_edge("run script at user logon", "psgallery.ps1")
G.add_edge("run script at user logon", "curl_customize.ps1")
G.add_edge("run script at user logon", "desktop_customize.ps1")
G.add_edge("run script at user logon", "git_customize.ps1")
G.add_edge("run script at user logon", "google_chrome_customize.ps1")
G.add_edge("run script at user logon", "powershell_core_customize.ps1")
G.add_edge("run script at user logon", "powershell_customize.ps1")
G.add_edge("run script at user logon", "powershell_customize_windows.ps1")
G.add_edge("run script at user logon", "taskbar_customize.ps1")

G.add_edge("vscode_customize.ps1", "vscode_minimize.ps1")

G.add_edge("vscode_autoit_customize.ps1", "vscode.ps1")

G.add_edge("vscode_minimize.ps1", "autoit.ps1")
G.add_edge("vscode_golang_customize.ps1", "vscode_minimize.ps1")
G.add_node("autoit.ps1")

G.add_edge("golang_customize.ps1", "golang.ps1")
G.add_edge("vscode_golang_customize.ps1", "golang_customize.ps1")
G.add_edge("vscode_golang_customize.ps1", "golang.ps1")
G.add_edge("vscode_golang_customize.ps1", "vscode.ps1")

G.add_edge("file_association_customize.ps1", "vscode.ps1")

G.add_edge("Microsoft.PowerShell_profile.ps1", "git.ps1")

G.add_edge("desktop_customize.ps1", "google_chrome.ps1")

G.add_edge("run script at user logon","rsat.ps1", style='dashed')
G.add_edge("run script at user logon","runonce registry key", style='dashed')
G.add_edge("run script at user logon","scheduled task", style='dashed')

G.add_node("packer.sh")
G.add_edge("packer.sh", "apt")

G.add_node("unzip")
G.add_edge("unzip", "setup.sh")

G.add_node("curl")
G.add_edge("curl", "setup.sh")

G.add_node("setup.sh")

G.add_edge("packer.sh", "bash")
G.add_edge("setup.sh", "bash")

G.add_edge("packer.sh", "apt")
G.add_edge("setup.sh", "apt")

G.add_node("packer.sh")

G.add_edge("taskbar_customize.ps1", "vscode.ps1")
G.add_edge("taskbar_customize.ps1", "google_chrome.ps1")
G.add_edge("taskbar_customize.ps1", "powershell_core_customize.ps1")
G.add_edge("taskbar_customize.ps1", "powershell_customize.ps1")
G.add_node("taskbar_customize.ps1")

G.add_node("add_user.ps1")
G.add_edge("add_user.ps1", "taskbar_customize.ps1")

G.add_edge("7zip.ps1", "psgallery.ps1")
G.add_edge("7zip.ps1", "chocolatey.ps1")

G.add_node("ec2config.ps1")
G.add_edge("ec2config.ps1", "7zip.ps1")

G.add_node("bazel.ps1")
G.add_edge("bazel.ps1", "chocolatey.ps1")

G.add_node("chocolatey.ps1")

G.add_edge("wget_customize.ps1", "wget.ps1")
G.add_edge("wget.ps1", "chocolatey.ps1")

G.add_edge("curl_customize.ps1", "curl.ps1")
G.add_edge("curl.ps1", "chocolatey.ps1")

G.add_edge("streambox_mediaplayer.ps1", "chocolatey.ps1")

G.add_edge("sysinternals.ps1", "chocolatey.ps1")

G.add_edge("git.ps1", "chocolatey.ps1")
G.add_edge("git_customize.ps1", "git.ps1")

G.add_node("google_chrome.ps1")
G.add_edge("google_chrome.ps1", "chocolatey.ps1")

G.add_edge("google_chrome_customize.ps1", "chocolatey.ps1")
G.add_edge("google_chrome_customize.ps1", "google_chrome.ps1")

G.add_edge("firefox.ps1", "chocolatey.ps1")

G.add_edge("perl.ps1", "chocolatey.ps1")

G.add_node("disable_uac.ps1")
G.add_node("network_customize.ps1")

G.add_node("openssh.ps1")

G.add_node("powershell_core.ps1")
G.add_edge("powershell_core.ps1", "chocolatey.ps1")

G.add_edge("powershell_core_customize.ps1", "powershell_core.ps1")

G.add_edge("openssh_with_powershell_core.ps1", "powershell_core.ps1")

G.add_node("openssh_with_powershell.ps1")

G.add_node("openssh_with_powershell_core.ps1")
G.add_edge("openssh_with_powershell_core.ps1", "openssh.ps1")

G.add_node("ping.ps1")
G.add_node("psgallery.ps1")

G.add_node("psgallery.ps1")
G.add_edge("psgallery.ps1", "nuget.ps1")

G.add_node("python.ps1")
G.add_edge("python.ps1", "chocolatey.ps1")

G.add_node("vscode.ps1")
G.add_edge("vscode.ps1", "chocolatey.ps1")

G.add_node("wixtoolset.ps1")
G.add_edge("wixtoolset.ps1", "chocolatey.ps1")

G.add_edge("windows_terminal.ps1", "chocolatey.ps1")

G.add_edge("sysprep.ps1", "ec2config.ps1")

G.add_edge("nuget.ps1", "dotnet_securityprotocol.ps1")
G.add_edge("chocolatey.ps1", "dotnet_securityprotocol.ps1")
G.add_edge("ec2config.ps1", "dotnet_securityprotocol.ps1")
G.add_edge("openssh.ps1", "dotnet_securityprotocol.ps1")
G.add_edge("pester.ps1", "dotnet_securityprotocol.ps1")
G.add_edge("psgallery.ps1", "dotnet_securityprotocol.ps1")

G.add_node("pester.ps1")
G.add_edge("pester.ps1", "nuget.ps1")

G.add_node("rdp")
G.add_edge("rdp", "creds_setup.sh")

"""
networkx hierarchy tree
networkx add_node predecessors
"""

networkx.nx_agraph.write_dot(G, "test.dot")

pos = networkx.drawing.nx_agraph.graphviz_layout(G, prog="dot")
networkx.draw(G, pos, with_labels=True, arrows=True)
matplotlib.pyplot.savefig("nx_test.png")

for node in list(G.nodes):
    print(node)
    print(list(networkx.edge_dfs(G, node)))
    print()

for node in G.nodes():
    print(networkx.degree_centrality(G)[node], node)
